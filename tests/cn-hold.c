#include "cnlib.h"
#include "cnobject/main.h"

int main() {
	CNDouble *num = cn_double_new(23.5);
	cn_life_up(num);

	cn_printf("num = %lf\n", num->value);

	/* Uso intensivo de CNHolds */
	cn_double_add(num, cn_double_new(2435.39));
	cn_double_add(num, cn_double_new(10));
	cn_double_div(num, cn_double_new(2));
	cn_double_sub(num, cn_double_new(10000));

	cn_printf("num = %lf\n", num->value);

	cn_life_down(num);

	cn_printf("Memory status: %lu\n", cnlib_mem_get_status());
	return 0;
}
