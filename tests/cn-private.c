#include "../cnlib.h"
#include "../cnobject/main.h"

int main() {
	/* CNString *cn_string_new();
	 * ≠
	 * CNString *cn_string_new_with(
	 *   const char *string); */
	CNString *str = cn_string_new_with("Test string");

	size_t cno__end = _cn_object__bound_end(cn_object_get_class(str));
	size_t cns__end = _cn_string__bound_end(cn_object_get_class(str));
	
	cn_printf("sizeof(str): %lu\n", cn_object_get_class(str)->INSTANCE_SIZE);
	cn_printf("((CNObject)str).__bound_end: %lu\n", cno__end);
	cn_printf("str.__bound_end: %lu\n", cns__end);
	cn_printf("str: %s\n", str->c_str);
	cn_printf("str.lifes: %lu\n", cn_life_get(str));

	cn_string_set(str, NULL);

	cn_printf("\nMemory status: %lu\n", cnlib_mem_get_status());

	cn_life_down(str);

	cn_printf("\nMemory status: %lu\n", cnlib_mem_get_status());

	return 0;
}