#include "cnlib.h"
#include <stdio.h>

int main() {
	int len;

	len = cn_print_len("Quantas letras isto tem?");
	cn_print("Quantas letras isto tem?");
	cn_printf("%d\n", len);

	cn_printf("Memory status: %lu\n", cnlib_mem_get_status());
	return 0;
}