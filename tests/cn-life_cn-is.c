#include <locale.h>
#include "../cnlib.h"
#include "../cnobject/main.h"

static void mexendo_com(CNString *gato) {
	cn_life_up(gato);
	CNString *otag = cn_string_reverse(gato);

	cn_printf("Número de vidas de 'gato': %lu\n", cn_life_get(gato));
	cn_printf("String 'gato' ao reverso: %s\n", otag->c_str);

	cn_life_down(otag);
	cn_life_down(gato);
}

int main() {
	setlocale(LC_ALL, "Portuguese");
	CNString *gato = cn_string_new_with("gato");

	cn_print("Para o método mexendo_com():");
	mexendo_com(gato);

	cn_print("De volta a main():");
	cn_printf("Vidas do gato: %lu\n", cn_life_get(gato));

	if (cn_is(gato, cn_object))
		cn_print("'gato' é um objeto");
	if (cn_is(gato, cn_string))
		cn_print("'gato' é uma cn_string");
	cn_life_down(gato);	

	if (!cn_is(gato, cn_string))
		cn_print("'gato' não é mais uma cn_string");

	cn_printf("Mem status: %lu\n", cnlib_mem_status_get());
	return 0;
}
