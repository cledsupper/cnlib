#include "../cnlib.h"
#include "../cnobject/main.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void stdin_flush() {
    char c = '\0';
    while (c != '\n' && c != EOF)
        c = getchar();
}

void cn_string_print(CNString *str) {
    printf("str: %s\n", str->c_str);
    printf("size: %lu\n", cn_string_size(str));
    printf("capacity: %lu\n", cn_string_capacity(str));
}

void cn_mem_print_status() {
    printf("Memory status must be 0: %lu\n", cnlib_mem_get_status());
}

int main() {
    char c_str[50];
    CNString *str = (CNString*) cn_new(CN_STRING);
    char c = '\n';

    printf("1. TEST STRING APPEND\n");
    printf("1.1. Appending char\n");
    srand(time(NULL));
    while (c == '\n') {
        char ch = rand()%54 + 'A';
        cn_string_append_char(str, ch);
        puts("");
        cn_string_print(str);

        printf("Press ENTER to continue, anything else to skip test\n");
        c = getchar();
    }
    printf("\nPress ENTER if needed\n");
    stdin_flush();

    printf("\n1.2. Appending strings\n");
    c = '\n';
    cn_string_set(str, NULL);
    while (c == '\n') {
        printf("\nType something:\n");
        scanf("%49[^\n]s", c_str);
        stdin_flush();

        cn_string_append(str, c_str);
        cn_string_print(str);

        printf("\nPress ENTER to continue, anything else to skip\n");
        c = getchar();
    }

    cn_life_down(str);

    printf("\nPress ENTER if needed\n");
    stdin_flush();

    cn_mem_print_status();
    printf("Press ENTER to continue test, anything else to quit\n");
    c = getchar();

    if (c != '\n')
        return 0;

    str = (CNString*) cn_new(CN_STRING);
    printf("\n2. TEST STRING PREPEND\n");
    printf("2.1. Prepending char\n");
    while (c == '\n') {
        char ch = rand()%54 + 'A';
        cn_string_prepend_char(str, ch);
        puts("");
        cn_string_print(str);

        printf("Press ENTER to continue, anything else to skip test\n");
        c = getchar();
    }

    printf("\nPress ENTER if needed\n");
    stdin_flush();

    printf("\n2.2. Prepending strings\n");
    c = '\n';
    cn_string_set(str, NULL);
    while (c == '\n') {
        printf("\nType something:\n");
        scanf("%49[^\n]s", c_str);
        stdin_flush();

        cn_string_prepend(str, c_str);
        cn_string_print(str);

        printf("\nPress ENTER to continue, anything else to skip\n");
        c = getchar();
    }

    printf("\nPress ENTER if needed\n");
    stdin_flush();

    cn_life_down(str);

    cn_mem_print_status();
    printf("Press ENTER to continue test, anything else to quit\n");
    c = getchar();

    if (c != '\n')
        return 0;

    str = (CNString*) cn_new(CN_STRING);
    printf("\n3. SETTING STRINGS\n");
    while (c == '\n') {
        printf("\nType something:\n");
        scanf("%49[^\n]", c_str);
        stdin_flush();

        cn_string_set(str, c_str);
        cn_string_print(str);

        printf("\nPress ENTER to continue, anything else to skip\n");
        c = getchar();
    }

    cn_life_down(str);

    cn_mem_print_status();
    return 0;
}
