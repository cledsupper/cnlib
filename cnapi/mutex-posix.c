/* mutex-posix.c - CNMutex POSIX implementation specific.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#include <time.h>
#include <math.h>
#include <errno.h>

static inline void init_error_set(int r, cn_mutex_error *e) {
  if (e) {
    switch (r) {
    case 0:
      *e = CN_MUTEX_ERROR_NONE;
      break;
    case EAGAIN:
    case ENOMEM:
      *e = CN_MUTEX_ERROR_MAX_RESOURCES;
      break;
    case EPERM:
      *e = CN_MUTEX_ERROR_NO_PERMISSION;
      break;
    case EBUSY:
      *e = CN_MUTEX_ERROR_LOCKED;
      break;
    default:
      *e = CN_MUTEX_ERROR_UNKNOWN;
    }
  }
}

bool cn_mutex_init(CNMutex *mutex, cn_mutex_error *e) {
  int r = pthread_mutex_init(&(mutex->lock), NULL);
  init_error_set(r, e);
  return !r;
}

bool cn_mutex_destroy(CNMutex *mutex, cn_mutex_error *e) {
  int r = pthread_mutex_destroy(&(mutex->lock));
  init_error_set(r, e);
  return !r;
}

static inline void lock_error_set(int r, cn_mutex_error *e) {
  if (e) {
    switch(r) {
      case 0:
        *e = CN_MUTEX_ERROR_NONE;
        break;
      case EINVAL:
        *e = CN_MUTEX_ERROR_INVALID;
        break;
      case EDEADLK:
        *e = CN_MUTEX_ERROR_DEADLOCK;
        break;
      case ETIMEDOUT:
        *e = CN_MUTEX_ERROR_TIMED_OUT;
        break;
      case EPERM:
        *e = CN_MUTEX_ERROR_STEALING;
        break;
      default:
        *e = CN_MUTEX_ERROR_UNKNOWN;
    }
  }
}

bool cn_mutex_lock(CNMutex *mutex, cn_mutex_error *e) {
  int r = pthread_mutex_lock(&(mutex->lock));
  lock_error_set(r, e);
  return !r;
}

bool cn_mutex_trylock(CNMutex *mutex, long ms, cn_mutex_error *e) {
  struct timespec timeout;
  int r;
  unsigned long ns_total;
  double ns_only = ms*pow(10, 6);

  clock_gettime(CLOCK_REALTIME, &timeout);

  ns_total = timeout.tv_nsec + ns_only;
  timeout.tv_nsec = ns_total%((unsigned long)pow(10, 9));

  timeout.tv_sec += ns_total/(unsigned long)pow(10, 9);

  r = pthread_mutex_timedlock(&(mutex->lock), &timeout);
  lock_error_set(r, e);
  return !r;
}

bool cn_mutex_unlock(CNMutex *mutex, cn_mutex_error *e) {
  int r = pthread_mutex_unlock(&(mutex->lock));
  lock_error_set(r, e);
  return !r;
}
