/* platform.h - Defines macros guessing operating system and includes its API main headers.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _CN_PLATFORM_H_
#define _CN_PLATFORM_H_

/* Default compiling is for a null platform */

/* All below, except Windows */
#define CN_ON_UNIX_FAMILY 0

#define CN_ON_ANDROID 0

/* OSX and iOS */
#define CN_ON_DARWIN 0

#define CN_ON_LINUX 0

#define CN_ON_WINDOWS 0


/* I'll redefine if I guess what OS is. OS { */
#if defined(unix) || defined(__unix) || defined(__unix__)

#undef CN_ON_UNIX_FAMILY
#define CN_ON_UNIX_FAMILY 1

/* Who Unix { */
#ifdef __ANDROID__
#undef CN_ON_ANDROID
#define CN_ON_ANDROID 1

#elif __APPLE__
#undef CN_ON_DARWIN
#define CN_ON_DARWIN 1

#elif defined(linux) || defined(__linux) || defined(__linux__)
#undef CN_ON_LINUX
#define CN_ON_LINUX 1

#else
#error I think Unix family of operating systems is too big for me
#endif /* } Who Unix */

#elif defined(_WIN32)
#undef CN_ON_WINDOWS
#define CN_ON_WINDOWS 1

#else
#error I can't guess what operating system you use :'(
#endif
/* } OS */


#if CN_ON_WINDOWS
#include <windows.h>

#else /* CN_ON_UNIX_FAMILY */
#include <unistd.h>
#endif

#endif
