/* mutex-windows.c - CNMutex Windows implementation specific.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
bool cn_mutex_init(CNMutex *mutex, cn_mutex_error *e) {
  mutex->lock = CreateMutex(NULL, FALSE, NULL);

  if (e) {
    if (mutex->lock == NULL)
      *e = CN_MUTEX_ERROR_UNKNOWN;
    else *e = CN_MUTEX_ERROR_NONE;
  }

  return lock != NULL;
}

bool cn_mutex_destroy(CNMutex *mutex, cn_mutex_error *e) {
  BOOL r = CloseHandle(mutex->lock);

  if (e) {
    if (r != 0)
      *e = CN_MUTEX_ERROR_UNKNOWN;
    else *e = CN_MUTEX_ERROR_NONE;
  }

  return r != 0;
}

static inline void lock_error_set(DWORD last_err, cn_mutex_error *e) {
  if (e)
    switch (last_err) {
      case WAIT_OBJECT_0:
        *e = CN_MUTEX_ERROR_NONE;
        break;
      case WAIT_ABANDONED:
        *e = CN_MUTEX_ERROR_ABANDONED;
        break;
      case WAIT_TIMEDOUT:
        *e = CN_MUTEX_ERROR_TIMEDOUT;
        break;
      default:
        *e = CN_MUTEX_ERROR_UNKNOWN;
    }
}

bool cn_mutex_lock(CNMutex *mutex, cn_mutex_error *e) {
  if (mutex->lock == NULL) {
    if (e)
      *e = CN_MUTEX_ERROR_INVALID;
    return false;
  }

  DWORD r = WaitForSingleObject(mutex->lock, INFINITE);
  lock_error_set(r, e);
  return r == WAIT_OBJECT_0;
}

bool cn_mutex_trylock(CNMutex *mutex, long ms, cn_mutex_error *e) {
  if (mutex->lock == NULL) {
    if (e)
      *e = CN_MUTEX_ERROR_INVALID;
    return false;
  }

  DWORD r = WaitForSingleObject(mutex->lock, ms);
  lock_error_set(r, e);
  return r == WAIT_OBJECT_0;
}

bool cn_mutex_unlock(CNMutex *mutex, cn_mutex_error *e) {
  if (mutex->lock == NULL) {
    if (e)
      *e = CN_MUTEX_ERROR_INVALID;
    return false;
  }

  BOOL r = ReleaseMutex(mutex->lock);
  if (!r)
    *e = CN_MUTEX_ERROR_UNKNOWN;
  return r != 0;
}
