/* mutex.h - crossplatform mutex.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _CN_MUTEX_H_
#define _CN_MUTEX_H_

#include "platform.h"
#include "../cn-c-types-reqds.h"

#if CN_ON_UNIX_FAMILY
#include <pthread.h>
#endif

typedef struct cn_mutex CNMutex;

struct cn_mutex {
#if CN_ON_WINDOWS
	HANDLE lock;
#elif CN_ON_UNIX_FAMILY
	pthread_mutex_t lock;
#endif
};

#ifndef __cplusplus
/** cn_mutex_error enum type */
typedef enum cn_mutex_error cn_mutex_error;
#endif

enum cn_mutex_error {
	CN_MUTEX_ERROR_NONE,
	/** UNIX only: can't create mutex due to hardware resource limits */
	CN_MUTEX_ERROR_MAX_RESOURCES,
	/** UNIX only: program has no permission for creating a mutex */
	CN_MUTEX_ERROR_NO_PERMISSION,
	/** UNIX only: mutex already exists and is locked by other thread */
	CN_MUTEX_ERROR_LOCKED,
	/** UNIX only: a deadlock condition was detected */
	CN_MUTEX_ERROR_DEADLOCK,

	/** Windows only: mutex abandoned. */
	CN_MUTEX_ERROR_ABANDONED,

	/** cn_mutex_unlock only: trying to unlock a mutex owned by another thread */
	CN_MUTEX_ERROR_STEALING,

	/** Mutex may not be initialized */
	CN_MUTEX_ERROR_INVALID,

	/** cn_mutex_trylock only: timed out */
	CN_MUTEX_ERROR_TIMED_OUT,

	/** See your platform documentation. */
	CN_MUTEX_ERROR_UNKNOWN
};

bool cn_mutex_init(CNMutex *mutex, cn_mutex_error *e);

bool cn_mutex_destroy(CNMutex *mutex, cn_mutex_error *e);

bool cn_mutex_lock(CNMutex *mutex, cn_mutex_error *e);

bool cn_mutex_trylock(CNMutex *mutex, long ms, cn_mutex_error *e);

bool cn_mutex_unlock(CNMutex *mutex, cn_mutex_error *e);


#endif
