/* thread-posix.c - CNThreads POSIX implementation specific.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#include <sched.h>

typedef struct cn_thread_emu_caller_args CNTECArgs;

struct cn_thread_emu_caller_args {
	CNThread *self;
	void* (*func)(void*);
	void *func_args;
	bool joinable;
	volatile bool get;
};

void *cn_thread_emu_caller(void *args) {
	CNTECArgs *src_args = (CNTECArgs*)args;
	CNTECArgs my_args = *src_args;
	src_args->get = true;

	void *ret_value = my_args.func(my_args.func_args);

	if (my_args.joinable == false)
		cn_free(my_args.self);

	return ret_value;
}

CNThread *cn_thread_new(void* (*foo)(void*),
			void *args,
			bool joinable) {

	pthread_t td;
	CNThread *thread = (CNThread*) _cn_alloc(1, sizeof(CNThread));

	CNTECArgs caller_args;

	caller_args.self = thread;
	caller_args.func = foo;
	caller_args.func_args = args;
	caller_args.joinable = joinable;
	caller_args.get = false;

	pthread_create(&td, NULL, cn_thread_emu_caller, &caller_args);

	if (!joinable) {
		pthread_detach(td);
		thread = NULL;
	}
	else thread->td = td;

	sched_yield();
	while (caller_args.get == false);
	return thread;
}

void *cn_thread_join(CNThread *thread) {
	void *ret_value;
	pthread_join(thread->td, &ret_value);
	cn_free(thread);
	return ret_value;
}
