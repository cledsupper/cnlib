#include "mutex.h"
#include <stdio.h>
#include <stdlib.h>

struct number_atomic {
  CNMutex mlock;
  int n;
} num = {.n = 0};

void *td_add(void *arg) {
  pthread_t td = pthread_self();
  cn_mutex_error e;
  cn_mutex_lock(&(num.mlock), &e);
  if (e != CN_MUTEX_ERROR_NONE) {
    fprintf(stderr, "Thread %lu: falha ao adicionar ao número!\n", td);
    return (void*)e;
  }
  else printf("Thread %lu: bloqueio conseguido\n", td);
  printf("Thread %lu: adicionando ao número...\n", td);
  num.n++;
  cn_mutex_unlock(&(num.mlock), &e);
  printf("Thread %lu: encerrando...\n", td);
  return (void*)e;
}

int main() {
  pthread_t tds[8];
  cn_mutex_error e;
  int t;
  cn_mutex_init(&(num.mlock), &e);
  if (e != CN_MUTEX_ERROR_NONE) {
    fprintf(stderr, "Erro ao inicialzar o CNMutex! (e = %d)\n", e);
    exit(e);
  }

  for (t=0; t < (sizeof(tds)/sizeof(*tds)); t++) {
    pthread_create(&tds[t], NULL, td_add, NULL);
  }

  printf("Esperando pelas threads terminarem...\n");
  for (t=0; t < (sizeof(tds)/sizeof(*tds)); t++) {
    void *ret;
    pthread_join(tds[t], &ret);
    if (ret != NULL)
      fprintf(stderr, "Thread %lu falhou com código de erro: %d\n", tds[t], (cn_mutex_error)ret);
  }

  cn_mutex_destroy(&(num.mlock), &e);
  if (e != CN_MUTEX_ERROR_NONE) {
    fprintf(stderr, "Erro ao destruir mutex (cod = %d)!!!\n", e);
    exit(e);
  }

  printf("Terminado\n");
  return 0;
}
