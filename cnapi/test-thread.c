#include "thread.h"
#include <stdio.h>

struct my_args_t {
  int a;
  int b;
};

static void *my_thread_func(void *args) {
  struct my_args_t *my_args = (struct my_args_t*) args;

  return (void*)(long)(my_args->a + my_args->b);
}

int main() {
  struct my_args_t my_args = {2, 5};
  CNThread *td = cn_thread_new(my_thread_func, &my_args, true);
  printf("Calculating...\n");
  int r = (int)(long) cn_thread_join(td);
  printf("= %d\n", r);
  return 0;
}
