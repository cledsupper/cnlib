/* mutex.c - CNMutex main source.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#include "mutex.h"

#if CN_ON_WINDOWS
#include "mutex-windows.c"
#else
#include "mutex-posix.c"
#endif
