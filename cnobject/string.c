/* string.c - Source of CNString.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#include "../cnlib.h"

#define CNStringConst 
#include "string.h"

#include <stdio.h>
#include <string.h>

typedef struct cn_string__ {
	char *a;

	/** Requested size for array */
	size_t s;

	/** Array actual capacity */
	size_t capacity;

	/* Please check my previous commit so you'll learn some good thing
	 * I have to delete in this commit :) */
} CNStringPrivate;

CN_DEFINE_WITH_PRIVATE(CNString, cn_string, cn_object)

static void cn_string_class_init(CNStringClass *classe) {
	CNObjectClass *o_class = as_cn_object_class(classe);

	o_class->copy = as_cn_copy(cn_string_copy);
	o_class->equals = as_cn_equals(cn_string_copy);
}

void cn_string_init(CNString *self) {
	CNStringPrivate *__ = cn_get__(self);
	self->c_str  = __->a = NULL;
	self->length = __->s = __->capacity = 0;
}

void cn_string_finalize(CNString *self) {
	CNStringPrivate *__ = cn_get__(self);
	if (__->a) {
		cn_free(__->a);
		__->a = NULL;
	}
}

static void cn_string_reflect(CNString *self) {
	CNStringPrivate *__ = cn_get__(self);
	self->c_str = __->a;
	self->length = __->s ? __->s-1 : 0;
}

static bool cn_string_resize_but_its_actually_a_true_resize(CNString *self, size_t n) {
	CNStringPrivate *__ = cn_get__(self);
	char *array;

	size_t capacity;

	/* A re-resize() must always resize the array */
	if (n == __->s)
		capacity = n;
	else
		capacity = cn_size_round(n, sizeof(*__));

	if (capacity != __->capacity) {
		array = cn_realloc(__->a, capacity);

		if (!array && capacity) {
			/* We reach an out-of-memory state */
			fprintf(stderr, "[ERROR] %s: no memory for CNString<%p> buffer!\n",
				__FILE__, self);
				return false;
		}
		__->a = array;
		__->capacity = capacity;
	}
	return true;
}

#define CNSTRING_PRINT_ERROR_AT(LINE_REL)\
 fprintf(stderr, "\t at %s<%d>: %s()",\
   __FILE__, (__LINE__+(LINE_REL)), __func__)

void cn_string_copy(CNString *self, CNString *src) {
	CNStringPrivate *__, *src__;

	cn_object.copy((CNObject*)self, (CNObject*)src);

	__ = cn_get__(self);
	src__ = cn_get__(src);

	if (src__->a) {
		if (cn_string_resize_but_its_actually_a_true_resize(self, src__->s)) {
			__->s = src__->s;
			memcpy(__->a, (src__->a), __->s);
		}
		else CNSTRING_PRINT_ERROR_AT(-4);
	}
	/* copy() não é utilizado somente para clonagem */
	else if (__->a) {
		cn_string_resize_but_its_actually_a_true_resize(self, 0);
		__->s = 0;
	}
	cn_string_reflect(self);
}

bool cn_string_equals(CNString *self, CNString *str) {
	if (cn_object.equals((CNObject*)self, (CNObject*)str)) {
		CNStringPrivate *__ = cn_get__(self);
		CNStringPrivate *str__ = cn_get__(str);

		if (__->s == str__->s)
			return !strncmp(__->a, str__->a, __->s);
	}

	return false;
}

CNString *cn_string_new() {
	return (CNString*) cn_new(CN_STRING);
}

CNString *cn_string_new_with(const char *c_str) {
	CNString *_new = cn_string_new();
	cn_string_set(_new, c_str);
	return _new;
}

CNString *cn_string_new_sized(size_t n, char pads) {
	CNString *_new = cn_string_new();
	cn_string_resize(_new, n, pads);
	return _new;
}

void cn_string_resize(CNString *self, size_t n, char pads) {
	CNStringPrivate *__ = cn_get__(self);

	if (n) {
		if (n > __->s) {
			if (pads) {
				size_t i = 0;
				if (cn_string_resize_but_its_actually_a_true_resize(self, n)) {
					if (__->s)
						__->a[__->s-1] = pads;
					/*for (i = __->size; n > i+1; i++)
						__->array[i] = pads;*/
					memset((__->a + __->s),
						   pads,
							n - __->s);
					__->a[n - 1] = 0;
					__->s = n;
				}
				else CNSTRING_PRINT_ERROR_AT(-11);
			}
		}
		else if (__->s > n) {
			cn_string_resize_but_its_actually_a_true_resize(self, n);
			__->a[n - 1] = 0;
			__->s = n;
		}
	}
	else if (__->a) {
		cn_free(__->a);
		__->a = NULL;
		__->capacity = __->s = 0;
	}
	cn_string_reflect(self);
}

void cn_string_set(CNString *self, const char *c_str) {
	CNStringPrivate *__ = cn_get__(self);
	size_t size;

	if (c_str) {
		size = 1+strlen(c_str);
		if (cn_string_resize_but_its_actually_a_true_resize(self, size)) {
			memcpy(__->a, c_str, size);
			__->s = size;
		}
		else CNSTRING_PRINT_ERROR_AT(-4);
	}
	else if (__->a) {
		cn_free(__->a);
		__->a = NULL;
		__->capacity = __->s = 0;
	}
	cn_string_reflect(self);
}

void cn_string_mount(CNString *self, char *c_str) {
	CNStringPrivate *__ = cn_get__(self);
	if (__->a) {
		cn_free(__->a);
		__->capacity = 0;
		__->s = 0;
	}

	__->a = c_str;

	if (c_str) {
		__->capacity = 1 + strlen(c_str);
		__->s = __->capacity;
	}
	else __->s = 0;

	cn_string_reflect(self);
}

CNString *cn_string_sub(CNString *self, long a, long z, int forward) {
	CNStringPrivate *__ = cn_get__(self);
	CNStringPrivate *new__;
	CNString *_new;
	long i,
		 self_length = __->s-1;
	size_t n;

	if (!forward) {
		cn_print_warn("treating 'forward' (= 0) as 1...");
		forward = 1;
	}

	if (a == z)
		return cn_string_new();

	else if (forward > 0) {
		if (a < 0 || z < 0)
			cn_printf_fatal("indexes 'a'(%ld) and 'z'(%ld) should be non-negative when 'forward'(%d) is positive!",
				a, z, forward);
		if (a >= self_length || z > self_length)
			cn_printf_fatal("Indexes for <%s at %p>[%ld:%ld:%d] out of bounds. String length is %ld",
				cn_object_get_class(self)->INSTANCE_NAME, self, a, z, forward, self_length);

		_new = cn_string_new();
		new__ = cn_get__(_new);

		n = (z-a)/forward + 1
				+ (forward>1 ? 1 : 0);
		new__->a = cn_alloc(char, n);
		if (!(new__->a)) {
			CNSTRING_PRINT_ERROR_AT(-2);
			cn_life_down(_new);
			return NULL;
		}
		else for (i=0; i*forward < z; i++)
				new__->a[i] = __->a[a+(i*forward)];
	}
	else {
		if (a >= 0 || z >= 0)
			cn_printf_fatal("Indexes 'a'(%ld) and 'z'(%ld) should be negative when 'forward'(%d) is negative!",
				a, z, forward);
		if (-a-1 >= self_length || -z-1 > self_length)
			cn_printf_fatal("Indexes for <%s at %p>[%ld:%ld:%d] out of bounds. String length is %ld",
				cn_object_get_class(self)->INSTANCE_NAME, self, a, z, forward, self_length);

		_new = cn_string_new();
		new__ = cn_get__(_new);

		forward *= -1;
		n = (a-z)/forward + 1
					+ (forward>1 ? 1 : 0);
		new__->a = cn_alloc(char, n);
		if (!(new__->a)) {
			CNSTRING_PRINT_ERROR_AT(-2);
			cn_life_down(_new);
			return NULL;
		}
		else for (i=0; i*forward < (0-z-1); i++)
				new__->a[i] = __->a[(self_length+a) - (i*forward)];
	}
	new__->a[i] = 0;
	new__->s = n;
	cn_string_reflect(_new);
	return _new;
}

CNString *cn_string_reverse(CNString *self) {
	CNStringPrivate *__ = cn_get__(self);
	return cn_string_sub(self, -1, -(__->s), -1);
}

void cn_string_append(CNString *self, const char *c_str) {
	if (c_str) {
		CNStringPrivate *__ = cn_get__(self);
		size_t c_str_len = strlen(c_str);
		size_t size_with_append;

		if (__->a) {
			size_with_append = __->s + c_str_len;
			if (cn_string_resize_but_its_actually_a_true_resize(self, size_with_append))
				memcpy((__->a + __->s - 1), c_str, c_str_len+1);
			else {
				CNSTRING_PRINT_ERROR_AT(-3);
				return;
			}
		}
		else {
			size_with_append = c_str_len+1;
			__->a = cn_alloc(char, size_with_append);
			if (!(__->a)) {
				CNSTRING_PRINT_ERROR_AT(-3);
				return;
			}
			memcpy(__->a, c_str, size_with_append);
			__->capacity = size_with_append;
		}
		__->s = size_with_append;
	}
	cn_string_reflect(self);
}

void cn_string_append_char(CNString *self, char c) {
	if (c) {
		CNStringPrivate *__ = cn_get__(self);
		if (__->a) {
			if (cn_string_resize_but_its_actually_a_true_resize(self, __->s+1)) {
				__->a[__->s-1] = c;
				__->a[__->s++] = 0;
			}
			else CNSTRING_PRINT_ERROR_AT(-4);
		}
		else {
			__->a = cn_alloc(char, 2);
			if (!(__->a))
				CNSTRING_PRINT_ERROR_AT(-2);
			else {
				__->a[0] = c;
				__->a[1] = 0;
				__->s = __->capacity = 2;
			}
		}
	}
	cn_string_reflect(self);
}

void cn_string_prepend(CNString *self, const char *c_str) {
	if (c_str) {
		CNStringPrivate *__ = cn_get__(self);
		size_t c_str_len = strlen(c_str);
		size_t size_with_prepend;

		if (__->a) {
			char *new_array;
			size_with_prepend = c_str_len + __->s;
			new_array = cn_alloc(char, size_with_prepend);
			if (!new_array) {
				CNSTRING_PRINT_ERROR_AT(-2);
				return;
			}
			else {
				memcpy(new_array, c_str, c_str_len);
				memcpy(new_array + c_str_len, __->a, __->s);

				cn_free(__->a);

				__->a = new_array;
				__->capacity = __->s = size_with_prepend;
			}
		}
		else {
			size_with_prepend = c_str_len + 1;
			__->a = cn_alloc(char, size_with_prepend);
			if (!(__->a))
				CNSTRING_PRINT_ERROR_AT(-2);
			else {
				memcpy(__->a, c_str, size_with_prepend);
				__->capacity = __->s = size_with_prepend;
			}
		}
	}
	cn_string_reflect(self);
}

void cn_string_prepend_char(CNString *self, char c) {
	if (c) {
		CNStringPrivate *__ = cn_get__(self);
		char *new_array;
		if (__->a) {
			new_array = cn_alloc(char, __->s+1);
			new_array[0] = c;
			memcpy(new_array+1, __->a, __->s);

			cn_free(__->a);
			__->a = new_array;
			++(__->s);
			__->capacity = __->s;
		}
		else {
			__->a = cn_alloc(char, 2);
			__->a[0] = c;
			__->a[1] = 0;
			__->capacity = __->s = 2;
		}
	}
	cn_string_reflect(self);
}

/* Está errado porque é redundante, mas está certo porque self->length pode ser indevidamente alterado. */
size_t cn_string_size(CNString *self) {
	return cn_get__(self)->s;
}

size_t cn_string_capacity(CNString *self) {
	return cn_get__(self)->capacity;
}
