/* object.h - Header of CNObject, the main type.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _CN_OBJECT_H_
#define _CN_OBJECT_H_

#ifndef CNObjectConst

#ifndef _CN_OBJECT_MAIN_
#warning YOU SHOULD NOT INCLUDE THIS HEADER DIRECTLY, prefer: <cnobject/main.h>
#endif

#define CNObjectConst const
#ifndef _CN_C_TYPES_REQDS_
#include "../cn-c-types-reqds.h"
#endif
#endif

#include "object-boilerplate.h"

CN_BEGIN_DECLS

typedef struct cn_object_class    CNObjectClass;
typedef struct cn_object_instance CNObject;

#define as_cn_init(PROCp)\
  ((void (*)(const CNObjectClass*, CNObject*))(PROCp))

#define as_cn_method(PROCp)\
  ((void (*)(CNObject*))(PROCp))

#define as_cn_copy(PROCEDp)\
  ((void (*)(CNObject*, CNObject*))(PROCEDp))

#define as_cn_equals(FUNCp)\
  ((bool (*)(CNObject*, CNObject*))(FUNCp))


/** CNObjectClass */
struct cn_object_class {
  /** This pointer can NEVER be changed on an inherited class. */
  const CNObjectClass *REAL;

  /** CLASS_NAME for runtime uses */
  const char *CLASS_NAME;
  /** CLASS_SIZE so we can check types for objects */
  size_t CLASS_SIZE;
  /** INSTANCE_NAME for runtime uses */
  const char *INSTANCE_NAME;
  /** INSTANCE_SIZE for creating objects. */
  size_t INSTANCE_SIZE;
  /** PRIVATE_SIZE for creating object private attributes. */
  size_t PRIVATE_SIZE;

  /** _init() is a fundamental virtual method, but it is already overrided when using CN_DEFINE. */
  void (*_init)(const CNObjectClass *classe, CNObject *obj);

  /** _finalize() is a fundamental virtual method, but it is already overriden when using CN_DEFINE. */
  void (*_finalize)(CNObject *self);

  /** copy() actually does nothing. */
  void (*copy)(CNObject *self, CNObject *src);

  /** equals() actually returns true, because CNObjects have nothing can distinguish them. */
  bool (*equals)(CNObject *self, CNObject *obj);

  /** What should happen if an object reach the EOL (End of Life) state?
    *
    * This is a virtual method which calls cn_object_delete for default, but you can override it carefully if you need.
    * You must call cn_delete(self) before your life_eol returns. */
  void (*life_eol)(CNObject *self);
};

extern const CNObjectClass cn_object;

/** CLASS_class_get() functions initializes the class chain until cn_object.
 * cn_object is a const class and does not need to be initialized. */
static inline const CNObjectClass *cn_object_class_get() {
	return &cn_object;
}

#define as_cn_object(CNOBJp)\
  ((CNObject*)(CNOBJp))

#define as_cn_object_class(CLASSEp)\
  ((CNObjectClass*)(CLASSEp))

#define cn_object_get_class(CNOBJp)\
  (((CNObject*)(CNOBJp))->CLASSE)


/** Where are MyObjectPrivate attributes?
 * It's right after superclass' private attributes.
 *
 * @param classe - an already initialized class */
uintptr_t _cn_object__bound_end(const CNObjectClass *classe);

/** Returns size of any object for this class, but actually the size of an instantiated object.
 *
 * @param classe - an already initialized class */
size_t cn_object_sizeof(const CNObjectClass *classe);


/** Helper for registering classes. */
void _cn_object_class_register(CNObjectClass *c,
			       const char *c_n,
			       size_t c_s,
			       const char *i_n,
			       size_t i_s,
				   size_t p_s);

/** Overrides fundamental virtual methods for a class. */
void _cn_object_class_override_fundamentals(
  CNObjectClass *c,
  void (*_i)(const CNObjectClass*, CNObject*),
  void (*_f)(CNObject*));

/** CNObject instance. */
struct cn_object_instance {
	void *CNObjectConst __OBJECT_META;
	const CNObjectClass *CNObjectConst CLASSE;
};

/** Helper for calling virtual method of object self.
 *
 * It's the same thing as calling:
 * |  cn_object_get_class(self)->copy(self, src);
 */
void cn_object_copy(CNObject *self, CNObject *src);

/** Helper for calling virtual method of object self.
 *
 * It's the same thing as calling:
 * |  cn_object_get_class(self)->equals(self, src); */
bool cn_object_equals(CNObject *self, CNObject *second);

/** Returns if self is a CNObject. */
bool cn_object_is(CNObject *self);

/** Instantiate an object.
 *
 * @param object_type - an already initialized class for new object.
 */
CNObject *cn_object_new(const CNObjectClass *object_type);

/** Finalizes and releases memory used by object. */
void cn_object_delete(CNObject *self);

/** Instantiates an object with the same type and attributes as self.
 * Virtual copy() must be implemented.
 */
CNObject *cn_object_clone(CNObject *self);

/** Increases one life for object.
 *
 * @returns self */
CNObject *cn_object_life_up(CNObject *self);

/** Tries to decrease life without deleting object immediately.
 * If object life was 0 before calling this method, it'll reach EOL state anyway (and probably be deleted).
 *
 * @returns self, or NULL if object reach the EOL state. */
CNObject *cn_object_life_halfdown(CNObject *self);

/** Decreases object life, calling life_eol() if object reach the EOL state.
 *
 * @returns self, or NULL if object reach the EOL state. */
CNObject *cn_object_life_down(CNObject *self);

/** Returns life count for object. */
uintmax_t cn_object_life_get(CNObject *self);

/** Helper for calling life_eol() */
void cn_object_life_eol(CNObject *self);

bool _cn_is_step(CNObject *self, const CNObjectClass *classe);

CN_END_DECLS


#define cn_new(OBJECT_CLASS_INITIALIZED) cn_object_new(OBJECT_CLASS_INITIALIZED)

#define cn_clone(CNOBJp) cn_object_clone((CNObject*)(CNOBJp))

#define cn_copy(SELFp, SRCp)\
  cn_object_copy((CNObject*)(SELFp), (CNObject*)(SRCp));

#define cn_equals(CNOBJp1, CNOBJp2)\
  (cn_object_equals((CNObject*)(CNOBJp1), (CNObject*)(CNOBJp2)))

#define cn_finalize(CNOBJp) cn_object_get_class(CNOBJp)\
  ->_finalize((CNObject*)(CNOBJp))

#define cn_delete(CNOBJp) cn_object_delete((CNObject*)(CNOBJp))

#define cn_life_up(CNOBJp) cn_object_life_up((CNObject*)(CNOBJp))

#define cn_life_halfdown(CNOBJp) cn_object_life_halfdown((CNObject*)(CNOBJp))

#define cn_life_down(CNOBJp) cn_object_life_down((CNObject*)(CNOBJp))

#define cn_life_get(CNOBJp) cn_object_life_get((CNObject*)(CNOBJp))

/** Returns size of any object as cnlib understands. */
#define cn_sizeof(CNOBJp) cn_object_sizeof(cn_object_get_class(CNOBJp))


#define _cn_is_strict(O, CLASS)\
  (CLASS == ((CNObject*)O)->CLASSE)

#define cn_is(CNOBJp, class_name)\
  (!cn_object_is((CNObject*)(CNOBJp)) ? false\
    : _cn_is_strict((CNOBJp), (class_name##_class_get())) ? true :\
      (_cn_is_step((CNObject*)(CNOBJp), class_name##_class_get()) ?\
        (CNObjectClass*)(class_name##_get_class(CNOBJp)->REAL) == class_name##_class_get() \
        : false))

#define cn_is_strict(CNOBJp, class_name)\
  (cn_object_is((CNObject*)(CNOBJp)) ?\
    _cn_is_strict((CNOBJp), (class_name##_class_get())) : false)

#endif
