/* string.h - Header of CNString, a string buffer type.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _CN_STRING_H_
#define _CN_STRING_H_

#ifndef CNStringConst
#define CNStringConst const
#else
#include "object.h"
#endif

CN_BEGIN_DECLS

#define CN_STRING (cn_string_class_get())

CN_DECLARE(CNString, cn_string)

extern CNStringConst struct cn_string_class {
CN_SUB(CNString, CNObject)
	/* In the future, CNString will become a CNHold. */
} cn_string;

struct cn_string_instance {
	CNObject super;

	/* Reflection of private value. */
	const char *c_str;
	long length;
};

/** cn_string's copy() implementation. */
void cn_string_copy(CNString *self, CNString *src);

/** cn_string's equals() implementation. */
bool cn_string_equals(CNString *self, CNString *str);

/** Creates a CNString with no array. */
CNString *cn_string_new();

/** Creates a CNString with a string.
 *
 * If 'c_str' is NULL: cn_string_new().
 *
 * @param c_str - C string. */
CNString *cn_string_new_with(const char *c_str);

/** Creates a CNString and fill empty spaces with 'pads'.
 *
 * cn_string_new_sized(10, 'A') => CNString("AAAAAAAAA");
 * cn_string_new_sized(10, '\0') => CNString(NULL);
 *
 * @param n	- array size.
 * @param pads	- padding characters. */
CNString *cn_string_new_sized(size_t n, char pads);

/** Resizes CNString, filling empty spaces with 'pads'.

 * If 'n' < old size:
 *   truncate.
 * If 'n' > old size  and 'pads' is not '\0':
 *   resizes and fill empty spaces with 'pads'.
 * If 'n' == 0:
 *   releases memory used for array.
 * Else:
 *   nothing
 *
 * @param n	- new size for array.
 * @param pads	- padding character.
 */
void cn_string_resize(CNString *self, size_t n, char pads);

/** Copy 'c_str' values into array.
 *
 * If 'c_str' is NULL:
 *   releases memory used for array.
 *
 * @param c_str - C string.
 */
void cn_string_set(CNString *self, const char *c_str);

/** Mount 'c_str' in CNString.
 *
 * @param c_str - C string allocated with cn_alloc().
 */
void cn_string_mount(CNString *self, char *c_str);

/** Generates CNString from 'start' to 'end'.
 * 
 * TODO: NOT WORKING, FIX THIS!
 *
 * If 'forward' is negative:
 *   string is read backwards and 'start' and 'end' must be negative indexes.
 *
 * @param start		- starting index.
 * @param end		- ending index (not included).
 * @param forward	- jumping 'forward' chars.
 */
CNString *cn_string_sub(CNString *self, long start, long end, int forward);

/** Exactly the same as:
 * | cn_string_sub(self, -1, -(self->length+1), -1); */
CNString *cn_string_reverse(CNString *self);

/** Appends C string to CNString. */
void cn_string_append(CNString *self, const char *c_str);

/** Appends a char to CNString. */
void cn_string_append_char(CNString *self, char c);

/** Prepends C string to CNString. */
void cn_string_prepend(CNString *self, const char *c_str);

/** Prepends a char to CNString. */
void cn_string_prepend_char(CNString *self, char c);

/** Returns string size. */
size_t cn_string_size(CNString *self);

/** Returns array capacity */
size_t cn_string_capacity(CNString *self);

CN_END_DECLS

#endif
