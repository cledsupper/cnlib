/* hold.h - Header of CNHold, an object for holding variables, pointers or objects.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _CN_HOLD_H_
#define _CN_HOLD_H_

#ifndef CNHoldConst
#define CNHoldConst const
#else
#include "../object.h"
#endif

CN_BEGIN_DECLS

CN_DECLARE(CNHold, cn_hold)

/** abstract CNHoldClass */
extern CNHoldConst struct cn_hold_class {
CN_SUB(CNHold, CNObject)

	/** Pure virtual _copy() must be overriden on subclasses */
	void (*_copy)(CNHold *self, CNHold *src);

	/** Actual len() is always 1, i.e., self holds one element. */
	size_t (*len)(CNHold *self);

	/** Actual compare() does comparing numbers for default. */
	long (*compare)(CNHold *self, CNHold *second);

	/** Actual greater() uses compare() for deciding which is the greater. */
	CNHold* (*greater)(CNHold *self, CNHold *second);

	/** Actual lesser() uses compare() for deciding which is the greater. */
	CNHold* (*lesser)(CNHold *self, CNHold *second);


	/** Actual add() does nothing. */
	void (*add)(CNHold *self, CNHold *second);
	/** Actual sub() does nothing. */
	void (*sub)(CNHold *self, CNHold *second);
	/** Actual mult() does nothing. */
	void (*mult)(CNHold *self, CNHold *second);
	/** Actual div() does nothing. */
	void (*div)(CNHold *self, CNHold *second);

	/** Actual to_bool().
	 * It does conversion based on C behavior, returning to_double() != 0.
	 * If self is not a number type, returns len(self) != 0. */
	bool (*to_bool)(CNHold *self);

	/** Actual to_int() is always 0. */
	int (*to_int)(CNHold *self);

	/** Actual to_long() is always 0. */
	long (*to_long)(CNHold *self);

	/** Actual to_float() is always Not-A-Number (NAN). */
	float (*to_float)(CNHold *self);

	/** Actual to_double() is always Not-A-Number (NAN). */
	double (*to_double)(CNHold *self);

	/** Actual to_string().
	 * If self holds a number, returns a newly allocated string with the number.
	 * Otherwise it returns a newly allocated string with object instance name and its pointer.
	 * 
	 * NOTE: returned array is temporary unless you set self->tmp0 = NULL.
	 */
	char* (*to_string)(CNHold *self);

	/** Actual to_pointer() returns NULL.
	 * 
	 * NOTE: returned pointer is temporary unless you set self->tmp0 = NULL.
	 */
	void* (*to_pointer)(CNHold *self);

	/** Actual to_object() clones self.
	 * 
	 * NOTE: returned object is temporary unless you set self->tmp1 = NULL.
	 */
	CNObject* (*to_object)(CNHold *self);

	/** Actual destroy() does nothing. */
	void (*destroy)(CNHold *self);
} cn_hold;

/** CNHold instance.
 * All CNHold types have no life since creation. */
struct cn_hold_instance {
	CNObject super;
	bool destroy_on_eol;

	/* Temporary data */
	void *tmp0;
	CNObject *tmp1;
};

typedef enum cn_hold_type {
	/** RAW type ables type change on subclasses. */
	CN_HOLD_TYPE_RAW,
	CN_HOLD_TYPE_NUMBER_INT,
	CN_HOLD_TYPE_NUMBER_REAL,
	CN_HOLD_TYPE_NUMBER_COMPLEX,
	CN_HOLD_TYPE_CHAR,
	CN_HOLD_TYPE_ARRAY,
	CN_HOLD_TYPE_STRING,
	CN_HOLD_TYPE_POINTER,
	CN_HOLD_TYPE_OBJECT
} cn_hold_type;

/** CNHold copy() implementation. */
void cn_hold_copy(CNHold *self, CNHold *src);

/** CNHold equals() implementation. */
bool cn_hold_equals(CNHold *self, CNHold *other);


/** Set type on subclass object initializer. */
void cn_hold_set_type(CNHold *self, cn_hold_type type);

/** Returns type of CNHold */
cn_hold_type cn_hold_get_type(CNHold *self);

#define as_cn_hold_copy(PROCp) ((void (*)(CNHold*, CNHold*))(PROCp))

/** virtual _copy() */
void cn_hold_copy_value(CNHold *self, CNHold *src);

/** virtual len() */
size_t cn_hold_len(CNHold *self); 

/** virtual compare() */
long cn_hold_compare(CNHold *self, CNHold *second);

#define as_cn_hold_greater(FUNCp) ((CNHold* (*)(CNHold*, CNHold*))(FUNCp))

/** virtual greater() */
CNHold *cn_hold_greater(CNHold *self, CNHold *second);

/** virtual lesser() */
CNHold *cn_hold_lesser(CNHold *self, CNHold *second);

/** virtual add() */
void cn_hold_add(CNHold *self, CNHold *second);

/** virtual sub() */
void cn_hold_sub(CNHold *self, CNHold *second);

/** virtual mult() */
void cn_hold_mult(CNHold *self, CNHold *second);

/** virtual div() */
void cn_hold_div(CNHold *self, CNHold *second);

/** virtual destroy() */
void cn_hold_destroy(CNHold *self);

#define as_cn_hold_to(FUNCp, RET_T) ((RET_T (*)(CNHold*))(FUNCp))

/** virtual to_bool() */
bool cn_hold_to_bool(CNHold *self);

/** virtual to_int() */
int cn_hold_to_int(CNHold *self);

/** virtual to_long() */
long cn_hold_to_long(CNHold *self);

/** virtual to_float() */
float cn_hold_to_float(CNHold *self);

/** virtual to_double() */
double cn_hold_to_double(CNHold *self);

/** If tmp0 != NULL: memory is free'd and pointer NULL.
 * If tmp1 != NULL: life down and pointer NULL. */
void cn_hold_clear_tmp(CNHold *self);

/** virtual to_string() */
char *cn_hold_to_string(CNHold *self);

/** virtual to_pointer() */
void *cn_hold_to_pointer(CNHold *self);

/** virtual to_object() */
CNObject *cn_hold_to_object(CNHold *self);

CN_END_DECLS

#endif
