/* hold.c - Source of CNHold.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#include "../../cnlib.h"
#define CNHoldConst 
#include "hold.h"

#include <stdlib.h>
#include <string.h>

typedef struct cn_hold__ {
	cn_hold_type type;
} CNHoldPrivate;

#define cn_check(HOLDp) cn_assert(cn_hold_is(HOLDp))

CN_DEFINE_WITH_PRIVATE(CNHold, cn_hold, cn_object)

/* Stubs for virtual methods { */
static void cn_hold_destroy_stub(CNHold *self) {}

static size_t cn_hold_len_stub(CNHold *self) { return 1; }

static void cn_hold_add_stub(CNHold *self, CNHold *other) {}

static int cn_hold_to_int_stub(CNHold *self) { return 0; }
static long cn_hold_to_long_stub(CNHold *self) { return 0L; }
static float cn_hold_to_float_stub(CNHold *self) { return strtof("NAN", NULL); }
static double cn_hold_to_double_stub(CNHold *self) { return strtod("NAN", NULL); }

static void *cn_hold_to_pointer_stub(CNHold *self) { return NULL; }
/* } Stubs for virtual methods */


static long cn_hold_compare_actual(CNHold*, CNHold*);
static CNHold *cn_hold_greater_actual(CNHold*, CNHold*);
static CNHold *cn_hold_lesser_actual(CNHold*, CNHold*);
static bool cn_hold_to_bool_actual(CNHold*);
static char *cn_hold_to_string_actual(CNHold*);
static CNObject *cn_hold_to_object_actual(CNHold*);


static void cn_hold_class_init(CNHoldClass *classe) {
	CNObjectClass *o_class = as_cn_object_class(classe);
	o_class->copy = as_cn_copy(cn_hold_copy);
	o_class->equals = as_cn_equals(cn_hold_equals);

	classe->destroy = cn_hold_destroy_stub;

	classe->_copy = NULL;
	classe->len = cn_hold_len_stub;
	classe->compare = cn_hold_compare_actual;
	classe->greater = cn_hold_greater_actual;
	classe->lesser = cn_hold_lesser_actual;

	classe->add = cn_hold_add_stub;
	classe->sub = cn_hold_add_stub;
	classe->mult = cn_hold_add_stub;
	classe->div = cn_hold_add_stub;

	classe->to_bool = cn_hold_to_bool_actual;
	classe->to_int = cn_hold_to_int_stub;
	classe->to_long = cn_hold_to_long_stub;
	classe->to_float = cn_hold_to_float_stub;
	classe->to_double = cn_hold_to_double_stub;

	classe->to_string = cn_hold_to_string_actual;

	classe->to_pointer = cn_hold_to_pointer_stub;
	classe->to_object = cn_hold_to_object_actual;
}

void cn_hold_init(CNHold *self) {
	cn_get__(self)->type = CN_HOLD_TYPE_RAW;
	self->destroy_on_eol = true;

	self->tmp0 = self->tmp1 = NULL;
	
	/* Every CNHold needs to be life_up() after created */
	self = (CNHold*)cn_life_halfdown(self);
	_cn_assert(self != NULL, __FILE__, __LINE__, __func__, "cn_life_halfdown(self)");
}

#define CLEAR_TMP(SELF)\
  if (SELF->tmp0) {\
    cn_free(SELF->tmp0);\
    SELF->tmp0 = NULL;\
  }\
  if (SELF->tmp1) {\
    cn_life_down(SELF->tmp1);\
    SELF->tmp1 = NULL;\
  }

void cn_hold_finalize(CNHold *self) {
	if (self->destroy_on_eol)
		cn_hold_get_class(self)->destroy(self);

	CLEAR_TMP(self);
}

void cn_hold_copy(CNHold *self, CNHold *src) {
	cn_object.copy(&(self->super), &(src->super));

	self->destroy_on_eol = src->destroy_on_eol;

	cn_hold_get_class(self)->_copy(self, src);
}

bool cn_hold_equals(CNHold *self, CNHold *other) {
	cn_check(other);
	cn_check(self);

	return !(cn_hold_get_class(self)->compare(self, other));
}


void cn_hold_set_type(CNHold *self, cn_hold_type type) {
	CNHoldPrivate *__;
	cn_check(self);
	__ = cn_get__(self);
	if (__->type == CN_HOLD_TYPE_RAW)
		__->type = type;
	else
		cn_printf_err("can not change type of a %s!", cn_object_get_class(self)->INSTANCE_NAME);
}

cn_hold_type cn_hold_get_type(CNHold *self) {
	return cn_get__(self)->type;
}

void cn_hold_copy_value(CNHold *self, CNHold *src) {
	cn_check(self);
	cn_hold_get_class(self)->_copy(self, src);
}

size_t cn_hold_len(CNHold *self) {
	cn_check(self);
	return cn_hold_get_class(self)->len(self);
}

long cn_hold_compare(CNHold *self, CNHold *second) {
	cn_check(self);
	return cn_hold_get_class(self)->compare(self, second);
}

CNHold *cn_hold_greater(CNHold *self, CNHold *second) {
	cn_check(self);
	return cn_hold_get_class(self)->greater(self, second);
}

CNHold *cn_hold_lesser(CNHold *self, CNHold *second) {
	cn_check(self);
	return cn_hold_get_class(self)->lesser(self, second);
}


void cn_hold_add(CNHold *self, CNHold *second) {
	cn_check(self);
    cn_hold_get_class(self)->add(self, second);
}

void cn_hold_sub(CNHold *self, CNHold *second) {
	cn_check(self);
    cn_hold_get_class(self)->sub(self, second);
}

void cn_hold_mult(CNHold *self, CNHold *second) {
	cn_check(self);
    cn_hold_get_class(self)->mult(self, second);
}

void cn_hold_div(CNHold *self, CNHold *second) {
	cn_check(self);
    cn_hold_get_class(self)->div(self, second);
}

int cn_hold_to_int(CNHold *self) {
	cn_check(self);
	return cn_hold_get_class(self)->to_int(self);
}

long cn_hold_to_long(CNHold *self) {
	cn_check(self);
	return cn_hold_get_class(self)->to_long(self);
}

float cn_hold_to_float(CNHold *self) {
	cn_check(self);
	return cn_hold_get_class(self)->to_float(self);
}

double cn_hold_to_double(CNHold *self) {
	cn_check(self);
	return cn_hold_get_class(self)->to_double(self);
}

bool cn_hold_to_bool(CNHold *self) {
	cn_check(self);
	return cn_hold_get_class(self)->to_bool(self);
}

void cn_hold_clear_tmp(CNHold *self) {
	cn_life_up(self);

	CLEAR_TMP(self);

	cn_life_down(self);
}

char *cn_hold_to_string(CNHold *self) {
	cn_check(self);
	return cn_hold_get_class(self)->to_string(self);
}

void *cn_hold_to_pointer(CNHold *self) {
	cn_check(self);
	return cn_hold_get_class(self)->to_pointer(self);
}

CNObject *cn_hold_to_object(CNHold *self) {
	cn_check(self);
	return cn_hold_get_class(self)->to_object(self);
}


/* Actual methods */
static long cn_hold_compare_actual(CNHold *self, CNHold *second) {
	cn_life_up(second);
	cn_life_up(self);

	long lself = cn_hold_to_long(self);
	long lsecond = cn_hold_to_long(second);

	cn_life_down(self);
	cn_life_down(second);
	return lsecond - lself;
}

static CNHold *cn_hold_greater_actual(CNHold *self, CNHold *second) {
	long r = cn_hold_get_class(self)->compare(self, second);
	return r > 0 ? second : self;
}

static CNHold *cn_hold_lesser_actual(CNHold *self, CNHold *second) {
	long r = cn_hold_get_class(self)->compare(self, second);
	return r < 0 ? second : self;
}

static bool cn_hold_to_bool_actual(CNHold *self) {
	switch (cn_get__(self)->type) {
		case CN_HOLD_TYPE_NUMBER_INT:
		case CN_HOLD_TYPE_NUMBER_REAL:
			return cn_hold_get_class(self)->to_double(self) != 0;

		default:
			return cn_hold_get_class(self)->len(self) != 0;
	}
}

static char *cn_hold_to_string_actual(CNHold *self) {
	cn_life_up(self);

	char *array = NULL;

	switch (cn_get__(self)->type) {
		case CN_HOLD_TYPE_NUMBER_INT:
			array = cn_aprintf(NULL, "%li", cn_hold_get_class(self)->to_long(self));
			break;

		case CN_HOLD_TYPE_NUMBER_REAL:
			array = cn_aprintf(NULL, "%lf", cn_hold_get_class(self)->to_double(self));
			break;

		default:
			array = cn_aprintf(NULL, "%s<at %p>", cn_object_get_class(self)->INSTANCE_NAME, self);
	}

	CLEAR_TMP(self);
	self->tmp0 = array;

	cn_life_down(self);

	return array;
}

static CNObject *cn_hold_to_object_actual(CNHold *self) {
	cn_life_up(self);

	CNObject *o_clone = cn_clone(self);

	CLEAR_TMP(self);
	self->tmp1 = o_clone;
	cn_object_life_up(o_clone);

	cn_life_down(self);
	return o_clone;
}
