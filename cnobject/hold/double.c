/* double.c - CNDouble main source.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#include "../../cnlib.h"
#include <stdio.h>

#define CNDoubleConst 
#include "double.h"

CN_DEFINE(CNDouble, cn_double, cn_hold)

static void cn_double_class_init(CNDoubleClass *classe) {
	CNHoldClass *h_class = as_cn_hold_class(classe);
	h_class->_copy = as_cn_hold_copy(cn_double_copy_value);
	h_class->greater = as_cn_hold_greater(cn_double_greater);
	h_class->lesser = as_cn_hold_greater(cn_double_lesser);

	h_class->add = as_cn_hold_copy(cn_double_add);
	h_class->sub = as_cn_hold_copy(cn_double_sub);
	h_class->mult = as_cn_hold_copy(cn_double_mult);
	h_class->div = as_cn_hold_copy(cn_double_div);

	h_class->to_int = as_cn_hold_to(cn_double_to_int, int);
	h_class->to_long = as_cn_hold_to(cn_double_to_long, long);
	h_class->to_float = as_cn_hold_to(cn_double_to_float, float);
	h_class->to_double = as_cn_hold_to(cn_double_to_double, double);

	h_class->to_pointer = as_cn_hold_to(cn_double_to_pointer, void*);
}

void cn_double_init(CNDouble *self) {
	cn_hold_set_type(&(self->super), CN_HOLD_TYPE_NUMBER_REAL);
}

void cn_double_finalize(CNDouble *self) {}

CNDouble *cn_double_new(double value) {
	CNDouble *self = (CNDouble*) cn_new(CN_DOUBLE);
	self->value = value;

	return self;
}

void cn_double_copy_value(CNDouble *self, CNHold *src) {
	cn_life_up(src);
	cn_life_up(self);
	self->value = cn_hold_to_double(src);
	cn_life_down(self);
	cn_life_down(src);
}

double cn_double_compare(CNDouble *self, CNHold *second) {
	cn_life_up(second);
	cn_life_up(self);
	double r = cn_hold_to_double(second) - self->value;
	cn_life_down(self);
	cn_life_down(second);
	return r;
}

CNHold *cn_double_greater(CNDouble *self, CNHold *second) {
	double r = cn_double_compare(self, second);
	return r > 0 ? second : (CNHold*)self;
}

CNHold *cn_double_lesser(CNDouble *self, CNHold *second) {
	double r = cn_double_compare(self, second);
	return r < 0 ? second : (CNHold*)self;
}

void cn_double_add(CNDouble *self, CNHold *second) {
	cn_life_up(second);
	cn_life_up(self);
	self->value += cn_hold_to_double(second);
	cn_life_down(self);
	cn_life_down(second);
}

void cn_double_sub(CNDouble *self, CNHold *second) {
	cn_life_up(second);
	cn_life_up(self);
	self->value -= cn_hold_to_double(second);
	cn_life_down(self);
	cn_life_down(second);
}

void cn_double_mult(CNDouble *self, CNHold *second) {
	cn_life_up(second);
	cn_life_up(self);
	self->value *= cn_hold_to_double(second);
	cn_life_down(self);
	cn_life_down(second);
}

void cn_double_div(CNDouble *self, CNHold *second) {
	cn_life_up(second);
	cn_life_up(self);
	self->value /= cn_hold_to_double(second);
	cn_life_down(self);
	cn_life_down(second);
}

int cn_double_to_int(CNDouble *self) {
	cn_life_up(self);
	int val = (int)(self->value);
	cn_life_down(self);
	return val;
}

long cn_double_to_long(CNDouble *self) {
	cn_life_up(self);
	long val = (long)(self->value);
	cn_life_down(self);
	return val;
}

float cn_double_to_float(CNDouble *self) {
	cn_life_up(self);
	float val = (float)(self->value);
	cn_life_down(self);
	return val;
}

double cn_double_to_double(CNDouble *self) {
	cn_life_up(self);
	double val = self->value;
	cn_life_down(self);
	return val;
}

double *cn_double_to_pointer(CNDouble *self) {
	cn_life_up(self);

	double *dp = cn_alloc(double, 1);
	*dp = self->value;

	cn_double_clear_tmp(self);
	as_cn_hold(self)->tmp0 = dp;

	cn_life_down(self);
	return dp;
}
