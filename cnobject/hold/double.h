/* double.h - Header of CNDouble, the main number type.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _CN_HOLD_DOUBLE_H_
#define _CN_HOLD_DOUBLE_H_

#ifndef CNDoubleConst
#define CNDoubleConst const
#else
#include "../object.h"
#include "hold.h"
#endif

CN_BEGIN_DECLS

#define CN_DOUBLE (cn_double_class_get())

CN_DECLARE(CNDouble, cn_double)

/** CNDoubleClass */
extern CNDoubleConst struct cn_double_class {
CN_SUB(CNDouble, CNHold)

} cn_double;

/** CNDouble instance */
struct cn_double_instance {
	CNHold super;

	double value;
};

/** Creates a CNDouble.
 * All CNHolds have no life since creation. */
CNDouble *cn_double_new(double value);

/** _copy() implementation */
void cn_double_copy_value(CNDouble *self, CNHold *src);

/** compare() exclusive */
double cn_double_compare(CNDouble *self, CNHold *second);

/** greater() exclusive */
CNHold *cn_double_greater(CNDouble *self, CNHold *second);

/** lesser() exclusive */
CNHold *cn_double_lesser(CNDouble *self, CNHold *second);

/** add() implementation */
void cn_double_add(CNDouble *self, CNHold *second);

/** sub() implementation */
void cn_double_sub(CNDouble *self, CNHold *second);

/** mult() implementation */
void cn_double_mult(CNDouble *self, CNHold *second);

/** div() implementation */
void cn_double_div(CNDouble *self, CNHold *second);

/** to_int() implementation */
int cn_double_to_int(CNDouble *self);

/** to_long() implementation */
long cn_double_to_long(CNDouble *self);

/** to_float() implementation */
float cn_double_to_float(CNDouble *self);

/** to_double() implementation */
double cn_double_to_double(CNDouble *self);

/** Helper */
static inline void cn_double_clear_tmp(CNDouble *self) {
	cn_hold_clear_tmp((CNHold*)self);
}

/** Helper */
static inline char *cn_double_to_string(CNDouble *self) {
	return cn_hold_get_class(self)->to_string((CNHold*)self);
}

/** to_pointer() implementation */
double *cn_double_to_pointer(CNDouble *self);

/** Helper */
static inline CNDouble *cn_double_to_object(CNDouble *self) {
	return (CNDouble*)(cn_hold_get_class(self)->to_object((CNHold*)self));
}

CN_END_DECLS

#endif
