/* object.c - Source of CNObject, the main type.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#include "../cnlib.h"
#define CNObjectConst 
#include "object.h"


static void cn_object_copy_actual(CNObject*, CNObject*);
static bool cn_object_equals_actual(CNObject*, CNObject*);

/** Our cn_object's private instance is just cn_size_round'ed */
typedef struct cn_object__ {
	/** life should be an atomic type, but atomic types are C11 features only. */
	volatile uintmax_t life;
} CNObjectPrivate;

static void _cn_object_init(const CNObjectClass*, CNObject*);
static void _cn_object_finalize(CNObject*);

const CNObjectClass cn_object = {
	&cn_object,

	"cn_object",
	sizeof(CNObjectClass),
	"CNObject",
	sizeof(CNObject),
	sizeof(CNObjectPrivate),

	_cn_object_init,
	_cn_object_finalize,
	cn_object_copy_actual,
	cn_object_equals_actual,
	cn_object_delete
};


uintptr_t _cn_object__bound_end(const CNObjectClass *classe) {
	return classe->INSTANCE_SIZE + sizeof(CNObjectPrivate);
}

size_t cn_object_sizeof(const CNObjectClass *classe) {
	return classe->INSTANCE_SIZE + classe->PRIVATE_SIZE;
}

void _cn_object_class_register(CNObjectClass *c,
			       const char *c_n,
			       size_t c_s,
			       const char *i_n,
			       size_t i_s,
			       size_t p_s) {
	c->CLASS_NAME = c_n;
	c->CLASS_SIZE = c_s;
	c->INSTANCE_NAME = i_n;
	c->INSTANCE_SIZE = cn_size_round(i_s, sizeof c);
	c->PRIVATE_SIZE += cn_size_round(p_s, sizeof c);
}

void _cn_object_class_override_fundamentals(
	CNObjectClass *c,
	void (*_i)(const CNObjectClass*, CNObject*),
	void (*_f)(CNObject*)) {
        c->_init = _i;
        c->_finalize = _f;
}


static inline CNObjectPrivate *cn_get__(CNObject *self) {
	return (CNObjectPrivate*)
		((uintptr_t)self + (uintptr_t)(self->CLASSE->INSTANCE_SIZE));
}

#define cn_check(OBJp) cn_assert(cn_object_is(OBJp))


static void *_cn_object_meta = NULL;

static void _cn_object_init(const CNObjectClass *classe, CNObject *obj) {
	if (!_cn_object_meta) {
		_cn_object_meta = (void*)((uintptr_t)obj + ((uintptr_t)(-1L)));
		if (!_cn_object_meta)
			_cn_object_meta = (void*)obj;
	}

	obj->__OBJECT_META = _cn_object_meta;

	obj->CLASSE = classe;

	CNObjectPrivate *__ = cn_get__(obj);
	__->life = 1;
}

static void _cn_object_finalize(CNObject *self) {
	self->__OBJECT_META = NULL;
}


static void cn_object_copy_actual(CNObject *self, CNObject *src) {}

static bool cn_object_equals_actual(CNObject *self, CNObject *second) {
	return true;
}

void cn_object_copy(CNObject *self, CNObject *src) {
	cn_check(self);
	cn_check(src);

	self->CLASSE->copy(self, src);
}

bool cn_object_equals(CNObject *self, CNObject *obj) {
	cn_check(self);
	cn_check(obj);

	return self->CLASSE->equals(self, obj);
}

bool cn_object_is(CNObject *self) {
	if (self)
		return self->__OBJECT_META == _cn_object_meta;
	return false;
}


CNObject *cn_object_new(const CNObjectClass *classe) {
	CNObject *self = (CNObject*) _cn_alloc(1, cn_object_sizeof(classe));
	classe->_init(classe, self);

	return self;
}

void cn_object_delete(CNObject *self) {
	cn_check(self);

	self->CLASSE->_finalize(self);
	cn_free(self);
}

CNObject *cn_object_clone(CNObject *self) {
	CNObject *o_clone;
	cn_check(self);
	o_clone = cn_object_new(self->CLASSE);

	o_clone->CLASSE->copy(o_clone, self);
	return o_clone;
}

CNObject *cn_object_life_up(CNObject *self) {
	cn_check(self);
	++(cn_get__(self)->life);
	return self;
}

CNObject *cn_object_life_halfdown(CNObject *self) {
	CNObjectPrivate *__;
	cn_check(self);

	__ = cn_get__(self);
	if (!__->life) {
		self->CLASSE->life_eol(self);
		return NULL;
	}
	--(__->life);
	return self;
}

CNObject *cn_object_life_down(CNObject *self) {
	CNObjectPrivate *__;
	cn_check(self);

	__ = cn_get__(self);

	if (__->life)
		--(__->life);

	if (!__->life) {
		self->CLASSE->life_eol(self);
		return NULL;
	}
	return self;
}

uintmax_t cn_object_life_get(CNObject *self) {
	cn_check(self);
	return cn_get__(self)->life;
}

void cn_object_life_eol(CNObject *self) {
	cn_check(self);
	self->CLASSE->life_eol(self);
}

bool _cn_is_step(CNObject *self, const CNObjectClass *classe) {
	return self->CLASSE->CLASS_SIZE >= classe->CLASS_SIZE;
}
