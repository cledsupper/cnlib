/* cnobject-boilerplate.h - Header of macros for defining new types.
 *
 * This file is part of cnlib.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * cnlib is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * cnlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _CN_OBJECT_BOILERPLATE_MACROS_
#define _CN_OBJECT_BOILERPLATE_MACROS_

#ifndef cn_size_round_defined
#define cn_size_round_defined 1

/** Rounds 's' up to be divisible by 'b', so (s%b) = 0. */
static inline size_t cn_size_round(size_t s, size_t b) {
	return s + (s%b != 0 ? (b - s%b) : 0);
}
#endif

/* CN_DECLARE { */

/** Declares all needed types, methods and macros needed for defining a CNObject type.
 * 
 * @param NamespaceObjName - friendly type name for class and instance (example: MyType)
 & @param namespace_obj_name - class name and even the namespace for class functions and instance methods (example: my_type)
 *
 * *** TYPES ***
 * typedef NamespaceObjName         - instance type;
 * typedef NamespaceObjNameClass    - class type;
 * 
 * *** as_*() ***
 * as_namespace_obj_name()          - cast for instance type;
 * as_namespace_obj_name_class()    - cast for class type;
 * namespace_obj_name_get_class()   - get object class;
 * 
 * *** functions, procedures and methods ***
 * namespace_obj_name_class_get()    - initializes and returns class
 * _namespace_obj_name__bound_end()  - for subclass cn_get__()
 * namespace_obj_name_init(self)     - your object default constructor (it's not namespace_obj_name._init!!!)
 * namespace_obj_name_finalize(self) - your object default destructor (it's not namespace_obj_name._finalize!!!)
 * namespace_obj_name_is(self)       - if 'self' class is (subclass of) namespace_obj_name.
 *
 * You must declare class and instance structs like these declarations below:
 * |
 * | extern NamespaceObjNameConst struct namespace_obj_name_class {
 * |    CNObjectClass super;
 * |    const NamespaceObjNameClass *REAL;
 * |
 * |    /.* Pure virtual methods should have a '_' prepending their names *./
 * |    void (*_pure_method)(NamespaceObjName *self);
 * |
 * |    void (*virtual_method1)(NamespaceObjName *self);
 * |    void (*virtual_method2)(NamespaceObjName *self);
 * |    ...
 * | } namespace_obj_name;
 * |
 * | struct namespace_obj_name_instance {
 * |    CNObject super;
 * |
 * |    int attribute;
 * |    int *dyn_attribute;
 * | };
 * 
 * NamespaceObjNameConst is a macro defined usually before CN_DECLARE:
 * | #ifndef NamespaceObjNameConst
 * | #define NamespaceObjNameConst const
 * | #endif
 * It's optional, but avoids others (except your class source code) of changing your class directly.
 * Note the class source will need to define NamespaceObjNameConst for nothing before including its class header. */
#define CN_DECLARE(NamespaceObjName, namespace_obj_name)\
\
typedef struct namespace_obj_name##_class    NamespaceObjName##Class;\
typedef struct namespace_obj_name##_instance NamespaceObjName;\
\
static inline NamespaceObjName *as_##namespace_obj_name(const void *p) {\
    return (NamespaceObjName*)p;\
}\
\
static inline NamespaceObjName##Class *as_##namespace_obj_name##_class(const void *p) {\
    return (NamespaceObjName##Class*)p;\
}\
\
static inline const NamespaceObjName##Class *namespace_obj_name##_get_class(const void *p) {\
    return (const NamespaceObjName##Class*)cn_object_get_class(p);\
}\
\
const CNObjectClass *namespace_obj_name##_class_get();\
\
uintptr_t _##namespace_obj_name##__bound_end(const CNObjectClass *classe);\
\
void namespace_obj_name##_init(NamespaceObjName *self);\
\
void namespace_obj_name##_finalize(NamespaceObjName *self);\
\
bool namespace_obj_name##_is(NamespaceObjName *self);
/* } CN_DECLARE */


/* CN_DEFINE { */

/** Defines all needed variables, methods and functions needed for a CNObject type.
 *
 * @param NamespaceObjName - friendly type name for class and instance (example: MyType)
 * @param namespace_obj_name - class name and even the namespace for class functions and instance methods (example: my_type)
 * @param namespace_super_name - class name and even the namespace of superclass functions and instance methods (example: cn_object)
 * 
 * *** Vars ***
 * namespace_obj_name  - public class instance.
 * __class_initialized - has 'namespace_obj_name' initialized?
 * 
 * *** Private procedures ***
 * _namespace_obj_name_class_init() - initializes public class instance
 * namespace_obj_name_class_init() - your class constructor
 * 
 * You must define your class' init procedure, and init and finalize methods for instance, including all needed pure virtual CNObject methods, like:
 * | static void virtual_method1_actual(NamespaceObjName *self) {
 * |   /.* default foo *./
 * | }
 * | static void virtual_method2_actual(NamespaceObjName *self) {
 * |   /.* default bar *./
 * | }
 * |
 * | static void namespace_obj_name_class_init(NamespaceObjNameClass *classe) {
 * |    /.* pure virtual *./
 * |    classe->_pure_method = NULL;
 | |
 * |    /.* virtual methods with default actions *./
 * |    classe->virtual_method1 = virtual_method1_actual;
 * |    classe->virutal_method2 = virtual_method2_actual;
 * |    ...
 * | }
 * |
 * | void namespace_obj_name_init(NamespaceObjName *self) {
 * |    self->attribute = 0;
 * |    self->dyn_attr = cn_alloc(int, 12);
 * | }
 * |
 * | void namespace_obj_name_finalize(NamespaceObjName *self) {
 * |    cn_free(self->dyn_attr);
 * | }
 * NOTE: only abstract classes should have pure virtual methods.
 */
#define CN_DEFINE(NamespaceObjName, namespace_obj_name, namespace_super_name)\
NamespaceObjName##Class namespace_obj_name;\
\
static void _##namespace_obj_name##_init(const CNObjectClass *classe, CNObject *obj) {\
    namespace_super_name##_class_get()->_init(classe, obj);\
    namespace_obj_name##_init((NamespaceObjName*)obj);\
}\
\
static void _##namespace_obj_name##_finalize(CNObject *self) {\
    namespace_obj_name##_finalize((NamespaceObjName*)self);\
    namespace_super_name##_class_get()->_finalize(self);\
}\
\
static void namespace_obj_name##_class_init(NamespaceObjName##Class*);\
\
static bool __class_initialized = false;\
static void _##namespace_obj_name##_class_init() {\
    \
    namespace_obj_name.super = namespace_super_name;\
    namespace_obj_name.REAL = &namespace_obj_name;\
    CNObjectClass *classe = as_cn_object_class(&namespace_obj_name);\
    _cn_object_class_register(classe,\
      #namespace_obj_name, sizeof(namespace_obj_name),\
      #NamespaceObjName, sizeof(NamespaceObjName),\
	  0);\
    \
    _cn_object_class_override_fundamentals(classe,\
      _##namespace_obj_name##_init,\
      _##namespace_obj_name##_finalize);\
    \
    namespace_obj_name##_class_init(&namespace_obj_name);\
}\
\
const CNObjectClass *namespace_obj_name##_class_get() {\
    if (!__class_initialized) {\
        namespace_super_name##_class_get();\
        _##namespace_obj_name##_class_init();\
        __class_initialized = true;\
    }\
    return (const CNObjectClass*)(&namespace_obj_name);\
}\
\
uintptr_t _##namespace_obj_name##__bound_end(const CNObjectClass *classe) {\
    return _##namespace_super_name##__bound_end(classe);\
}\
\
bool namespace_obj_name##_is(NamespaceObjName *self) {\
    return cn_is(self, namespace_obj_name);\
}

/* } CN_DEFINE */


/* CN_DEFINE_WITH_PRIVATE { */

/** Same as CN_DEFINE, but including private data.
 * 
 * *** Private methods ***
 * cn_get__(self) - returns self's private data.
 *
 * You should have declared a struct for holding private data like the example below, right before using this macro:
 * | typedef struct {
 * |     const char *priv_attribute;
 * |     /.* The padding below is merely optional and only for the ones whose want a well fine sorted memory on your objects *./
 * |     void *_padn;
 * | } NamespaceObjNamePrivate;
 *
 * @param NamespaceObjName - friendly type name for class and instance (example: MyType)
 * @param namespace_obj_name - class name and even the namespace for class functions and instance methods (example: my_type)
 * @param namespace_super_name - class name and even the namespace of superclass functions and instance methods (example: cn_object)
 *
 * See CN_DEFINE() documentation.
 */
#define CN_DEFINE_WITH_PRIVATE(NamespaceObjName, namespace_obj_name, namespace_super_name)\
NamespaceObjName##Class namespace_obj_name;\
\
static NamespaceObjName##Private *cn_get__(NamespaceObjName *self) {\
    return (NamespaceObjName##Private*)(_##namespace_super_name##__bound_end(((CNObject*)self)->CLASSE)\
             + (uintptr_t)self);\
}\
\
static void _##namespace_obj_name##_init(const CNObjectClass *classe, CNObject *obj) {\
    namespace_super_name._init(classe, obj);\
    namespace_obj_name##_init((NamespaceObjName*)obj);\
}\
\
static void _##namespace_obj_name##_finalize(CNObject *self) {\
    namespace_obj_name##_finalize((NamespaceObjName*)self);\
    namespace_super_name._finalize(self);\
}\
\
static void namespace_obj_name##_class_init(NamespaceObjName##Class*);\
\
static bool __class_initialized = false;\
static void _##namespace_obj_name##_class_init() {\
    namespace_obj_name.super = namespace_super_name;\
    namespace_obj_name.REAL = &(namespace_obj_name);\
    \
    CNObjectClass *classe = as_cn_object_class(&namespace_obj_name);\
    _cn_object_class_register(classe,\
      #namespace_obj_name, sizeof(namespace_obj_name),\
      #NamespaceObjName, sizeof(NamespaceObjName),\
	  sizeof(NamespaceObjName##Private));\
    _cn_object_class_override_fundamentals(classe,\
      _##namespace_obj_name##_init,\
      _##namespace_obj_name##_finalize);\
    \
    namespace_obj_name##_class_init(&namespace_obj_name);\
}\
\
const CNObjectClass *namespace_obj_name##_class_get() {\
    if (!__class_initialized) {\
        namespace_super_name##_class_get();\
        _##namespace_obj_name##_class_init();\
        __class_initialized = true;\
    }\
    return (const CNObjectClass*)(&namespace_obj_name);\
}\
\
uintptr_t _##namespace_obj_name##__bound_end(const CNObjectClass *classe) {\
	return _##namespace_super_name##__bound_end(classe) +\
        cn_size_round(sizeof(NamespaceObjName##Private), sizeof classe);\
}\
\
bool namespace_obj_name##_is(NamespaceObjName *self) {\
    return cn_is(self, namespace_obj_name);\
}
/* } CN_DEFINE_WITH_PRIVATE */


/** Helper for declaring class structs
 * See CN_DEFINE() documentation. */
#define CN_SUB(NamespaceObjName, NamespaceSuperName)\
  NamespaceSuperName##Class super;\
  const NamespaceObjName##Class *REAL;

#ifndef CN_BEGIN_DECLS
/* If you want source portability between C and C++, you should use CN__DECLS */
#ifdef __cplusplus
#define CN_BEGIN_DECLS extern "C" {
#define CN_END_DECLS }
#else
#define CN_BEGIN_DECLS 
#define CN_END_DECLS 
#endif
#endif

#endif
