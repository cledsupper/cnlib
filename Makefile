###########  CNLIB MAKEFILE v. 0.6  ###########

# Test shared library on Termux
#INSTALL_LIB_DIR=/data/data/com.termux/files/usr/lib
#INSTALL_HEADERS_DIR=/data/data/com.termux/files/usr/include/cnlib

# Test shared library on GNU/Linux
INSTALL_LIB_DIR=/usr/local/lib
INSTALL_HEADERS_DIR=/usr/local/include/cnlib

LIB_NAME=libcnlib
LIB_NAME_VER_LINKER=cnlib-0.6
LIB_NAME_VER=lib$(LIB_NAME_VER_LINKER).200116.so
LIB_NAME_VER_BASE=lib$(LIB_NAME_VER_LINKER).so


all: cnlib make-cnobject

test: dirs
	$(CC) -o bin/test test.c lib/*.o

cnlib: dirs
	$(CC) -c cnlib.c
	mv cnlib.o lib/

make-cnobject: dirs
	cd cnobject && $(MAKE)

test-shared:
	$(CC) test.c -l$(LIB_NAME_VER_LINKER) -o bin/test

shared: cnlib-shared make-cnobject-shared
	$(CC) -shared -o lib/libcnlib.so lib/*.o

cnlib-shared: dirs
	$(CC) -fPIC -c cnlib.c
	mv cnlib.o lib/

make-cnobject-shared: dirs
	cd cnobject && $(MAKE) shared

dirs:
	mkdir -p bin
	mkdir -p lib

clean:
	rm -f lib/*.o lib/*.so
	rm -f bin/test


install: install-headers install-lib

uninstall: uninstall-headers uninstall-lib

install-lib:
	mv lib/libcnlib.so $(INSTALL_LIB_DIR)/$(LIB_NAME_VER)
	ln -s $(INSTALL_LIB_DIR)/$(LIB_NAME_VER) $(INSTALL_LIB_DIR)/$(LIB_NAME_VER_BASE)
	# Ignore errors below when not compiling on Linux
	chown root:root $(INSTALL_LIB_DIR)/$(LIB_NAME_VER)
	ldconfig

uninstall-lib:
	rm $(INSTALL_LIB_DIR)/$(LIB_NAME)*

install-headers:
	mkdir $(INSTALL_HEADERS_DIR)
	mkdir -p $(INSTALL_HEADERS_DIR)/cnobject/hold
	cp cnlib.h $(INSTALL_HEADERS_DIR)/
	cp cn-c-types-reqds.h $(INSTALL_HEADERS_DIR)/
	cp cnobject/*.h $(INSTALL_HEADERS_DIR)/cnobject/
	cp cnobject/hold/*.h $(INSTALL_HEADERS_DIR)/cnobject/hold/
	# Some headers have no reading permission for other users, so we have to grant this
	chmod --recursive +r $(INSTALL_HEADERS_DIR)

uninstall-headers:
	rm $(INSTALL_HEADERS_DIR)/cnlib.h
	rm $(INSTALL_HEADERS_DIR)/cn-c-types-reqds.h
	rm -R $(INSTALL_HEADERS_DIR)/cnobject
	rmdir $(INSTALL_HEADERS_DIR)
