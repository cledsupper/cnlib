#include "cnlib.h"
#include "cnobject/main.h"

int main() {
	CNString *str = (CNString*) cn_new(CN_STRING);

	cn_string_set(str, "AeadoedmeoZ");
	cn_printf("%s\n", str->c_str);

	CNString *rts = cn_string_reverse(str);
	cn_printf("%s\n", rts->c_str);
	cn_life_down(rts);

	rts = cn_string_sub(str, 2, str->length, 1);
	cn_printf("%s\n", rts->c_str);
	cn_life_down(rts);

	rts = cn_string_sub(str, -1, -9, -1);
	cn_printf("%s\n", rts->c_str);
	cn_life_down(rts);

	cn_life_down(str);
	cn_printf("Memory status must be 0: %lu\n", cnlib_mem_get_status());
	return 0;
}
