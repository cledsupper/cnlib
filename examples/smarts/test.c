#include "autoprint.h"

#include <stdio.h>
#include <string.h>

int main() {
	char text[100];
	AutoPrint *lag_logger = (AutoPrint*) cn_new(AUTO_PRINT);

	/* CORREÇÃO: todos os CNObjects já possuem uma vida
	 * desde a sua criação, com exceção daqueles baseados
	 * em CNHold. */

	do {
		printf("Type anything, or \"quit\" to exit: ");
		scanf("%99[^\n]s", text);
		getchar();

		auto_print_set(lag_logger, text);
	} while (strcmp(text, "quit"));

	/* As vidas de um objeto são equivalentes à contagem de
	 * referências de um coletor de lixo conservador: cada
	 * cada função que possuir uma referência ao objeto
	 * deve incrementar o número de vidas do objeto e
	 * decrementar antes de dar o retorno à função anterior.
	 *
	 * MÉTODOS CN_LIFE PARA CONTAGEM DE REFERÊNCIAS
	 * • cn_life_up() - incrementa uma vida no objeto.
	 * • cn_life_halfdown() - decrementa uma vida sem cham-
	 * ar o método life_eol(), exceto no caso de o objeto
	 * não possuir nenhuma vida.
	 * • cn_life_down() - decrementa uma vida, caso possua,
	 * chamando life_eol() se acabaram as vidas do objeto.
	 * • cn_life_get() - retorna o número de vidas do
	 * objeto. */

	cn_life_down(lag_logger);
	return 0;
}
