#ifndef _AUTO_PRINT_H_
#define _AUTO_PRINT_H_

#ifndef AutoPrintConst
#define AutoPrintConst const
#endif

#include <cnlib/cnobject/main.h>

CN_BEGIN_DECLS

#define AUTO_PRINT (auto_print_class_get())

CN_DECLARE(AutoPrint, auto_print)

struct auto_print_class {
CN_SUB(AutoPrint, CNObject)
};

struct auto_print_instance {
	CNObject super;
	void *__;
};

void auto_print_set(AutoPrint *self, const char *text);

void auto_print_life_eol(AutoPrint *self);

CN_END_DECLS

#endif
