#define AutoPrintConst 
#include "autoprint.h"

#include <cnlib/cnlib.h>
#include <pthread.h>
#include <unistd.h>

#ifndef __cplusplus
typedef enum auto_print_state auto_print_state;
#endif

enum auto_print_state {
	AUTO_PRINT_OK,
	AUTO_PRINT_CHANGED,
	AUTO_PRINT_END,
	AUTO_PRINT_ENDED
};

typedef struct {
	pthread_t thread;
	const char *text;
	volatile auto_print_state state;
} AutoPrintPrivate;

CN_DEFINE_WITH_PRIVATE(AutoPrint, auto_print, cn_object)

static void auto_print_class_init(AutoPrintClass *classe) {
	CNObjectClass *o_class = as_cn_object_class(classe);
	o_class->life_eol = as_cn_method(auto_print_life_eol);
}

static void *auto_print_thread(void*);

void auto_print_init(AutoPrint *self) {
	AutoPrintPrivate *__ = cn_get__(self);
	__->text = NULL;

	pthread_create(&(__->thread), NULL, auto_print_thread, self);
}

void auto_print_finalize(AutoPrint *self) {
	AutoPrintPrivate *__ = cn_get__(self);
	cn_assert(__->state == AUTO_PRINT_ENDED);
}

static void *auto_print_thread(void *self) {
	AutoPrintPrivate *__ = cn_get__((AutoPrint*)self);
	__->state = AUTO_PRINT_OK;
	while (__->state != AUTO_PRINT_END) {
		if (__->state == AUTO_PRINT_CHANGED) {
			__->state = AUTO_PRINT_OK;
			if (__->text)
				cn_printf("%s\n", __->text);
		}
		usleep(500000);
	}
	__->state = AUTO_PRINT_ENDED;
	cn_printf_log("Thread of %s<%p> terminated", cn_object_get_class(self)->INSTANCE_NAME, self);
	return NULL;
}


void auto_print_set(AutoPrint *self, const char *text) {
	AutoPrintPrivate *__ = cn_get__(self);
	__->text = text;
	__->state = AUTO_PRINT_CHANGED;
}

void auto_print_life_eol(AutoPrint *self) {
	AutoPrintPrivate *__ = cn_get__(self);
	__->state = AUTO_PRINT_END;
	pthread_join(__->thread, NULL);

	cn_object.life_eol(&(self->super));
}
