/** test.c - Program to test all objects. */
#include "animal-cat.h"
#include "dog.h"
#include <cnlib/cnlib.h>

int main() {
	AnimalCat *cat = animal_cat_new("Luci");
	Dog *dog = dog_new("Brutus");
	CNString *teste = cn_string_new_with("teste");

	animal_cat_speak(cat);
	dog_speak(dog);
	animal_speak((Animal*)dog);

	animal_set_name((Animal*)dog, "will");

	if (!cn_is(NULL, animal))
		cn_print("\nNULL não é animal!\n");
	if (cn_is(cat, animal_cat))
		cn_printf("%s é um gato\n", animal_get_name(as_animal(cat)));
	if (!cn_is(dog, animal_cat))
		cn_printf("%s não é um gato\n", animal_get_name(as_animal(dog)));
	if (!cn_is(cat, animal))
		cn_printf("%s herda de animal e é um %s\n",
			animal_get_name(as_animal(cat)), cn_object_get_class(cat)->CLASS_NAME);
	if (cn_is(cat, cn_object))
		cn_print("Esse gato também herda de objeto\n");
	if (!cn_is(teste, animal)) {
		cn_print("Teste não é um animal\n");
		if (cn_is(teste, cn_string))
			cn_print("Teste é uma CNString\n");
	}

	cn_life_down(dog);
	cn_print("Dog died\n");
	cn_life_down(cat);
	cn_print("Cat died\n");
	cn_life_down(teste);
	cn_print("Teste deleted\n");

	cn_printf("Allocated blocks: %lu\n", cnlib_mem_get_status());
	return 0;
}
