/** dog.h - An example of final class using cnlib's CNObject base, but without doing use of namespaces.
 * It's not recommended to declare and define objects without namespace, because this will confuse others whose are searching for class' methods, but we can still do it. */
#ifndef _DOG_H_
#define _DOG_H_

#include "animal.h"

#ifndef DogConst
#define DogConst const
#endif

CN_BEGIN_DECLS

/** We will call cn_new(DOG) on main source for creating a dog.
 * dog_class_get() initializes all needed classes if they were still not initialized, and returns DogClass as a CNObjectClass, so we can create a Dog with cn_object_new() function.
 */
#define DOG (dog_class_get())

CN_DECLARE(Dog, dog)

extern DogConst struct dog_class {
CN_SUB(Dog, Animal)

	/* We will only implement the speak() method on dog source */
} dog;

struct dog_instance {
	Animal super;
};

/** Creates a dog with a name already defined. */
Dog *dog_new(const char *name);

/** Our speak() implementation.
 * Perhaps we can call it directly, we could call animal_speak() too if we ant.
 * Check animal.c and dog.c, so you will understand why.  */
void dog_speak(Dog *self);

/** Helper for calling superclass' get_name() method from dog class. */
static inline const char *dog_get_name(Dog *self) {
	return animal_get_name(&(self->super));
}

/* We don't have a dog_set_name(). This is because we don't recommend the use of that method on a dog. */

CN_END_DECLS

#endif
