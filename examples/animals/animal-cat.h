/** animal-cat.h - An example of final class using cnlib's CNObject base. */
#ifndef _ANIMAL_CAT_H_
#define _ANIMAL_CAT_H_

#include "animal.h"

/* This macro excludes others from overwriting our class methods. */
#ifndef AnimalCatConst
#define AnimalCatConst const
#endif

CN_BEGIN_DECLS

/** We will call cn_new(ANIMAL_CAT) on main source for creating a dog.
 * animal_cat_class_get() initializes all needed classes if they were still not initialized, and returns AnimalCatClass as a CNObjectClass for creating an AnimalCat with cn_object_new() function.
 */
#define ANIMAL_CAT (animal_cat_class_get())

CN_DECLARE(AnimalCat, animal_cat)

extern AnimalCatConst struct animal_cat_class {
CN_SUB(AnimalCat, Animal)

	/* We will only implement the speak() method */
} animal_cat;

struct animal_cat_instance {
	Animal super;
};

/** Creates a cat with a name already defined. */
AnimalCat *animal_cat_new(const char *name);

/** A helper for superclass' speak() method.
 * Note the use of 'static' and 'inline' keywords. */
static inline void animal_cat_speak(AnimalCat *self) {
	/* First way of casting for superclass' method: &(self->super). */
	return animal_speak(&(self->super));
}

/** A helper for superclass' get_name() method.
 * Note the use of 'static' and 'inline' keywords.
 */
static inline const char *animal_cat_get_name(AnimalCat *self) {
	/* Second way of casting for superclass' method: as_animal(self). */
	return animal_get_name(as_animal(self));
}

/* We don't have a animal_cat_set_name(). This is because we don't recommend the use of that method on an animal_cat. */

CN_END_DECLS

#endif
