/** animal.h - An example of abstract class using cnlib's CNObject base. */
#ifndef _ANIMAL_H_
#define _ANIMAL_H_

#include <cnlib/cnobject/main.h>

#ifndef AnimalConst
#define AnimalConst const
#endif

/* Use CN_BEGIN_DECLS for allowing safe inclusion on C++ codes. */
CN_BEGIN_DECLS

/* Declares all needed types and methods for animal class and instance. */
CN_DECLARE(Animal, animal)

extern AnimalConst struct animal_class {
CN_SUB(Animal, CNObject)

	/** animal.speak() is a pure virtual method.
	 * Our animal doesn't know how to speak, so the program will abort with a segmentation fault if we try to call it from a pure animal instance. */
	void (*speak)(Animal *self);
} animal;

struct animal_instance {
	CNObject super;
};

void animal_copy(Animal *self, Animal *other);

/** Helper for calling self.speak() for object 'self' */
void animal_speak(Animal *self);

/** Sets animal name one time only. */
void animal_set_name(Animal *self, const char *name);

/** Returns name of the animal. */
const char *animal_get_name(Animal *self);

CN_END_DECLS

#endif
