#define AnimalConst 
#include "animal.h"

#include <cnlib/cnlib.h>
#include <stdio.h>
#include <string.h>

typedef struct {
	char name[100];
	bool isset;
} AnimalPrivate;

CN_DEFINE_WITH_PRIVATE(Animal, animal, cn_object)

static void animal_class_init(AnimalClass *classe) {
	classe->super.copy = as_cn_copy(animal_copy);

	classe->speak = NULL;
}

void animal_init(Animal *self) {
	static int animals_created = 0;
	AnimalPrivate *__ = cn_get__(self);
	__->isset = false;
	snprintf(__->name, 100, "Animal %d", ++animals_created);
}

void animal_finalize(Animal *self) {
	// nobody cares to a destructor
}

void animal_copy(Animal *self, Animal *src) {
	AnimalPrivate *__ = cn_get__(self);
	AnimalPrivate *src__ = cn_get__(src);

	strncpy(__->name, src__->name, 99);
	__->name[99] = '\0';

	cn_object.copy(&(self->super), &(src->super));
}

void animal_speak(Animal *self) {
	animal_get_class(self)->speak(self);
}

void animal_set_name(Animal *self, const char *name) {
	AnimalPrivate *__ = cn_get__(self);

	if (__->isset) {
		cn_printf_err("<%p>.name is already set: %s", self, __->name);
		return;
	}
	__->isset = true;
	strncpy(__->name, name, 99);
}

const char *animal_get_name(Animal *self) {
	return cn_get__(self)->name;
}
