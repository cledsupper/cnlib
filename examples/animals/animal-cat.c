#define AnimalCatConst 
#include "animal-cat.h"

#include <cnlib/cnlib.h>

CN_DEFINE(AnimalCat, animal_cat, animal)

/* We have to declare the virtual method before using it (C rule). */
static void animal_cat_speak_actual(Animal *self);

static void animal_cat_class_init(AnimalCatClass *classe) {
	/* We don't have to cast function pointer since this implementation is compatible with superclass' speak pointer. */
	classe->super.speak = animal_cat_speak_actual;

	/* If this final class wanted to override a grandparent class virtual method, we would need to cast 'classe' using as_grandparent_class() casting function, for example:
	 * | CNObjectClass *cno_class = as_cn_object_class(classe);
	 * | cno_class->life_eol = animal_cat_life_eol_actual;
	 * */
}

/* This is the default constructor for AnimalCat, always called after object initializes, especially after the superclass' constructor. */
void animal_cat_init(AnimalCat *self) {}

/* This is the destructor for self, called before object memory be cn_free'd, and even before the superclass' destructor. */
void animal_cat_finalize(AnimalCat *self) {}


AnimalCat *animal_cat_new(const char *name) {
	AnimalCat *self = (AnimalCat*) cn_new(ANIMAL_CAT);
	animal_set_name(&(self->super), name);
	return self;
}

static void animal_cat_speak_actual(Animal *self) {
	cn_printf("%s says: meowww\n", animal_get_name(self));
}
