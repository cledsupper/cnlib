#define DogConst 
#include "dog.h"

#include <cnlib/cnlib.h>

CN_DEFINE(Dog, dog, animal)

static void dog_class_init(DogClass *classe) {
	/* Overrides animal.speak. Note the pointer cast for this case. */
	classe->super.speak = (void (*)(Animal*)) dog_speak;
}

/* This is the default constructor for Dog, always called after object initializes, especially after the superclass' constructor. */
void dog_init(Dog *self) {}

/* This is the destructor for self, called before object memory be cn_free'd, and even before the superclass' destructor. */
void dog_finalize(Dog *self) {}


Dog *dog_new(const char *name) {
	Dog *self = (Dog*) cn_new(DOG);
	animal_set_name(&(self->super), name);
	return self;
}

void dog_speak(Dog *self) {
	cn_printf("%s says: WOOF!\n", animal_get_name(&(self->super)));
}
