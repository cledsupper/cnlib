/* cnlib.c - A library providing O.O.P schemes for C, and other facilities.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#include "cnlib.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

CN_ON_DEBUG(
void _cn_assert(bool expr,
			   const char *src_file,
			   int src_line,
			   const char *src_func,
			   const char *error_message) {
	if (!expr)
		_cn_printf(CN_PRINT_FATAL, src_file, src_line, src_func, "assertion (%s) fail!", error_message);
}
)

void cn_abort() {
	abort();
}

char *cn_vaprintf(int *l, const char *format, va_list vl) {
	int size = 0;
	char *outstr = NULL;
	va_list vtry;
	if (!l)
		l = &size;
	*l = 0;

	va_copy(vtry, vl);
	size = vsnprintf(outstr, size, format, vtry);
	va_end(vtry);

	if (size < 0) {
		*l = size;
		return outstr;
	}
	++size;

	outstr = (char*) _cn_alloc(size, sizeof(char));
	*l = vsnprintf(outstr, size, format, vl);
	return outstr;
}

char *cn_aprintf(int *l, const char *format, ...) {
	char *outstr;
	va_list vl;
	va_start(vl, format);

	outstr = cn_vaprintf(l, format, vl);

	va_end(vl);

	return outstr;
}


int _cn_print(cn_print_option option,
			  const char *src_file,
			  int src_line,
			  const char *src_func,
			  const char *message) {
	char option_char = 'W';
	int length;

	if (option == CN_PRINT_OK || option == CN_PRINT_LENGTH)
		length = strlen(message);

	if (option == CN_PRINT_LENGTH)
		return length;

	switch (option) {
		case CN_PRINT_OK:
			fwrite(message, sizeof(char), length, stdout);
			break;
		case CN_PRINT_LOG:
			option_char = 'L';
		case CN_PRINT_WARNING:
			length = printf("[%c] %s<%d> at %s(): %s\n", option_char,
				src_file, src_line, src_func,
				message);
			break;
		case CN_PRINT_ERROR:
			length = fprintf(stderr, "[ERROR] %s<%d> at %s(): %s\n",
				src_file, src_line, src_func,
				message);
			break;
		case CN_PRINT_FATAL:
			length = fprintf(stderr, "[FATAL] %s<%d> at %s(): %s\n",
				src_file, src_line, src_func,
				message);
			break;
		default:
			break;
	}
	if (option == CN_PRINT_FATAL)
		abort();
	return length;
}

int _cn_printf(cn_print_option option,
			   const char *src_file,
			   int src_line,
			   const char *src_func,
			   const char *format, ...) {
	char *outstr = NULL;
	int length;
	va_list vl;

	va_start(vl, format);
	if (option == CN_PRINT_LENGTH) {
		length = vsnprintf(outstr, 0, format, vl);
		va_end(vl);
	}
	else {
		int fmtted_length;
		outstr = cn_vaprintf(&fmtted_length, format, vl);
		va_end(vl);

		cn_assert(fmtted_length >= 0 || option != CN_PRINT_FATAL);
		if (!outstr)
			return fmtted_length;

		char option_char = 'W';

		switch (option) {
			case CN_PRINT_OK:
				fwrite(outstr, sizeof(char), fmtted_length, stdout);
				break;
			case CN_PRINT_LOG:
				option_char = 'L';
			case CN_PRINT_WARNING:
				length = printf("[%c] %s<%d> at %s(): %s\n", option_char,
					src_file, src_line, src_func,
					outstr);
				break;
			case CN_PRINT_ERROR:
				length = fprintf(stderr, "[ERROR] %s<%d> at %s(): %s\n",
					src_file, src_line, src_func,
					outstr);
				break;
			case CN_PRINT_FATAL:
				length = fprintf(stderr, "[FATAL] %s<%d> at %s(): %s\n",
					src_file, src_line, src_func,
					outstr);
				break;
			default:
				break;
		}
		cn_free(outstr);
		if (option == CN_PRINT_FATAL)
			abort();
	}
	return length;
}

CN_ON_DEBUG(
static struct {
	volatile uintmax_t mem_status;
} cnlib = {0};
)

void *_cn_alloc(size_t n_elems, size_t s) {
	size_t size = n_elems * s;
	cn_assert(size != 0);
	CN_ON_DEBUG(++cnlib.mem_status);
	return malloc(size);
}

void *_cn_alloc0(size_t n_elems, size_t s) {
	size_t size = n_elems * s;
	cn_assert(size != 0);
	CN_ON_DEBUG(++cnlib.mem_status);
	return calloc(n_elems, s);
}

void *_cn_realloc(void *p, size_t n_elems, size_t s) {
	size_t new_size = n_elems * s;
	if (p && new_size)
		return realloc(p, new_size);

	if (!p && !new_size)
		return p;

	if (!p)
		return _cn_alloc(n_elems, s);

	cn_free(p);
	return NULL;
}

void *_cn_realloc_if_greater(void *p, size_t n_elems, size_t s, size_t n_elems_before) {
	size_t old_size = n_elems_before * s;
	size_t new_size = n_elems * s;
	if (new_size <= old_size && new_size)
		return p;

	if (!new_size) {
		cn_free(p);
		return NULL;
	}

	if (!old_size)
		return _cn_alloc(n_elems, s);

	return realloc(p, new_size);
}

void cn_free(void *p) {
	cn_assert(p != NULL);
	CN_ON_DEBUG(--cnlib.mem_status);
	free(p);
}

uintmax_t cnlib_mem_get_status() {
	uintmax_t ret = -1;
	CN_ON_DEBUG(ret = cnlib.mem_status;)
	return ret;
}
