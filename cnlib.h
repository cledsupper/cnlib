/* cnlib.h - A library providing O.O.P schemes for C, and other facilities.
 *
 * Copyright (C) 2014-2020  Cledson F. Cavalcanti
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _CNLIB_H_
#define _CNLIB_H_

#include "cn-c-types-reqds.h"
#include <stdarg.h>

/* NDEBUG { */
#ifndef NDEBUG

#define CN_ON_DEBUG(...) __VA_ARGS__

void _cn_assert(bool expr,
			   const char *src_file,
			   int src_line,
			   const char *src_func_name,
			   const char *error_message);

#define cn_assert(EXPRESSION)\
  _cn_assert((EXPRESSION), __FILE__, __LINE__, __func__, #EXPRESSION )

#define cn_print_log(LOG_MESSAGE)\
  _cn_print(CN_PRINT_LOG, __FILE__, __LINE__, __func__, LOG_MESSAGE)

#define cn_printf_log(FORMAT, ...)\
  _cn_printf(CN_PRINT_LOG, __FILE__, __LINE__, __func__, FORMAT ,##__VA_ARGS__)

#else

#define CN_ON_DEBUG(...) /* ._. */
#define _cn_assert(...) /* o_o */
#define cn_assert(...) /* oh wait... */
#define cn_print_log(...) /* O _ O */
#define cn_printf_log(...) /* `(0) v (0)´ */

#endif /* } NDEBUG */


void cn_abort();

#ifndef cn_size_round_defined
#define cn_size_round_defined 1

/** Rounds 's' up to be divisible by b, so (s%b) = 0. */
static inline size_t cn_size_round(size_t s, size_t b) {
	return s + (s%b != 0 ? (b - s%b) : 0);
}
#endif

/** Creates a string formatted like as with vsprintf(), but returned array is cn_alloc'd and has the size of resulting string.
 *
 * If 'len' != NULL, it sets the variable pointed by len to the result of vsnprintf().
 *
 * If an encoding error occurred, len will be a negative number and NULL will be returned.
 *
 * @return formatted string. */
char *cn_vaprintf(int *len, const char *format, va_list vl);

/** See cn_vaprintf() documentation. */
char *cn_aprintf(int *len, const char *format, ...)
  __attribute__ ((format (printf, 2, 3)));


#ifndef __cplusplus
typedef enum cn_print_option cn_print_option;
#endif

enum cn_print_option {
	CN_PRINT_OK,
	CN_PRINT_LOG,
	CN_PRINT_WARNING,
	CN_PRINT_ERROR,
	CN_PRINT_FATAL,
  CN_PRINT_LENGTH
};

/** _cn_print() prints a message in according to 'option'.
 * If option is CN_PRINT_OK, just prints the message like fputs(message, stdout) was called.
 * Basic format of all non-CN_PRINT_OK messages is:
 * | [FLAG] FILE.c<N>: at FUNCTION_NAME(): MESSAGE\n
 * 
 * If option is CN_PRINT_LENGTH, _cn_print() will only return the length of 'message'.
 * 
 * NOTE: CN_PRINT_FATAL => abort().
 * 
 * @return return of printf, or strlen(message).
 */
int _cn_print(cn_print_option option,
				   const char *src_file,
				   int src_line,
				   const char *src_func,
				   const char *message);

/** Like _cn_print(), but formatting string too.
 *
 * If an encoding error occurs while formatting message, the behavior will depend if 'option' is set for CN_PRINT_FATAL:
 * Case CN_PRINT_FATAL, aborts imediately and nothing is shown.
 * Otherwise, returns the 'len' value set by cn_vaprintf().
 *
 * See _cn_print() and cn_vaprintf() documentation.
 */
int _cn_printf(cn_print_option option,
			   const char *src_file,
			   int src_line,
			   const char *src_func,
			   const char *format, ...)
	__attribute__ ((format (printf, 5, 6)));


/** Just like malloc(), but counting all blocks allocated.
 * 
 * You can get the number of blocks currently allocated via cnlib calling cnlib_mem_get_status().
 * 
 * @return a newly allocated memory block with requested size. This block should be freed with cn_free().
 */
void *_cn_alloc(size_t n_elems, size_t s);

/** Just like calloc(), but counting all blocks allocated.
 * 
 * @return a newly allocated memory block with requested size. This block should be freed with cn_free().
 *
 * See _cn_alloc() documentation.
 */
void *_cn_alloc0(size_t n_elems, size_t s);

/** Resizes block like realloc() does, but with some gurantees:
 * 
 * If 'p' is NULL and size (n_elems * s) == 0:
 *   returns NULL.
 * If 'p' is NULL and size (n_elems * s) != 0:
 *   _cn_alloc(n_elems, s).
 * If 'p' not NULL and size = 0:
 *   cn_free(p);
 * Else:
 *   realloc(p, n_elems*s);
 *
 * See _cn_alloc() documentation.
 */
void *_cn_realloc(void *p, size_t n_elems, size_t s);


/** The same behavior of _cn_realloc(), but it does nothing if 'n_elems' is less than 'n_elems_before'.
 *
 * This can result in some benefic bug preventing too much reallocs in memory.
 *
 * See _cn_realloc() documentation.
 */
void *_cn_realloc_if_greater(void *p, size_t n_elems, size_t s, size_t n_elems_before);


/** Calls free(p) and decreases cnlib's mem count.
 * See _cn_alloc() and cnlib_mem_get_status() documentation.
 */
void cn_free(void *p);


/** Returns the number of blocks allocated with cnlib. */
uintmax_t cnlib_mem_get_status();

/** Helper for _cn_print() */
#define cn_print(LOG)\
  _cn_print(CN_PRINT_OK, NULL, 0, NULL, LOG)

/** Helper for _cn_printf() */
#define cn_printf(FORMAT, ...)\
  _cn_printf(CN_PRINT_OK, NULL, 0, NULL, FORMAT ,##__VA_ARGS__ )

/** Helper for _cn_print() */
#define cn_print_warn(WARNING_MESSAGE)\
  _cn_print(CN_PRINT_WARNING, __FILE__, __LINE__, __func__, WARNING_MESSAGE)

/** Helper for _cn_printf() */
#define cn_printf_warn(FORMAT, ...)\
  _cn_printf(CN_PRINT_WARNING, __FILE__, __LINE__, __func__, FORMAT ,##__VA_ARGS__ )

/** Helper for _cn_print() */
#define cn_print_err(ERROR_MESSAGE)\
  _cn_print(CN_PRINT_ERROR, __FILE__, __LINE__, __func__, ERROR_MESSAGE)

/** Helper for _cn_printf() */
#define cn_printf_err(FORMAT, ...)\
  _cn_printf(CN_PRINT_ERROR, __FILE__, __LINE__, __func__, FORMAT ,##__VA_ARGS__ )

/** Helper for _cn_print() */
#define cn_print_fatal(ERROR_MESSAGE)\
  _cn_print(CN_PRINT_FATAL, __FILE__, __LINE__, __func__, ERROR_MESSAGE)

/** Helper for _cn_printf() */
#define cn_printf_fatal(FORMAT, ...)\
  _cn_printf(CN_PRINT_FATAL, __FILE__, __LINE__, __func__, FORMAT ,##__VA_ARGS__ )

#define cn_print_len(LOG)\
  _cn_print(CN_PRINT_LENGTH, NULL, 0, NULL, LOG)

#define cn_printf_len(FORMAT, ...)\
  _cn_printf(CN_PRINT_LENGTH, NULL, 0, NULL, FORMAT ,##__VA_ARGS__ )


/* Avoids redundancy and heap usage, but requires stdio.h to be included */

#define print_cn_log(LOG)\
  printf("[L] %s<%d>: at %s(): %s\n",\
    __FILE__, __LINE__, __func__, LOG);

#define print_cn_err(LOG, ...)\
  fprintf(stderr, "[ERROR] %s<%d>: at %s(): %s\n",\
    __FILE__, __LINE__, __func__, LOG);

#define print_cn_fatal(LOG, ...)\
  { fprintf(stderr, "[FATAL] %s<%d>: at %s(): %s\n",\
      __FILE__, __LINE__, __func__, LOG);\
    cn_abort();\
  }


#define cn_alloc(TYPE, N_ELEMS)\
 ((TYPE*) _cn_alloc((N_ELEMS), sizeof(TYPE)))

#define cn_alloc0(TYPE, N_ELEMS)\
 ((TYPE*) _cn_alloc0((N_ELEMS), sizeof(TYPE)))

/* Avoid using these macros if you want portability between platforms and compilers */
#define cn_realloc(P, N_ELEMS)\
 ((__typeof__(P)) _cn_realloc((P), (N_ELEMS), sizeof(*(P))))

#define cn_realloc_if_greater(P, N_ELEMS, N_ELEMS_BEFORE)\
 ((__typeof__(P)) _cn_realloc_if_greater((P), (N_ELEMS), sizeof(*(P)), (N_ELEMS_BEFORE)))


#endif
